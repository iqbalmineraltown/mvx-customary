# Selamat datang di Mobile Apps Team!

## Panduan Mulai Cepat

Pertama kali di Xamarin? Apa yang perlu dipasang? Cara mengupgrade?
Silahkan baca panduan mulai cepat: https://devs.peentar.com/w/development/mobile/getting_started/

## Membuat Fitur

Pembuatan fitur di Mobile Apps dilandasi atas pemikiran Lego. Beberapa
komponen umum bisa dipakai. Komponen khusus menempati fungsinya masing-masing.
Sama halnya seperti membuat prakarya dengan Lego, Anda mengerti cara membuat
atap, tetapi ada atap khusus yang menggunakan panel surya; akan tetapi mereka
tetaplah atap dan memiliki fungsi menutupi bangunan dari atas.

Pelajari pembuatan fitur pada wiki Building Block: https://devs.peentar.com/w/development/mobile/building_block/

## Panduan Penamaan

Penamaan memiliki pembahasan tersendiri karena berkaitan dengan struktur,
konsistensi, pembagian tanggung jawab, dan domain.

Pelajari panduan penamaan pada wiki Naming Guide: https://devs.peentar.com/w/development/mobile/development_guide/naming/

## Panduan Pembangunan

Untuk membangun aplikasi, proyek ini menggunakan `CakeBuild` yang diatur dalam berkas `.cake`. Pengaturan tersebut dijalankan melalui **Windows Powershell**.

Untuk pembangunan khusus pengembang, cukup menjalankan:
```
.\build.ps1
```

Sedangkan untuk pembangunan uji / rilis:
```
.\build.ps1 -Target CiBuild -Configuration <env>
```

Kemudian untuk menggunggah ke TestFairy:
```
.\build.ps1 -Target TfUpload -Configuration <env>
```

Kedua perintah sebelumnya dapat dijalankan secara prosedural dengan:
```
.\build.ps1 -Target TfJenkins -Configuration <env>
```

Nilai `<env>` harus diganti dengan salah satu dari:
- `Debug` (Default jika tidak menggunakan opsi `-Configuration`)
- `Staging`
- `Demo`
- `Release`

Pengaturan pembangunan dapat diubah pada berkas `AppConfig.json`.