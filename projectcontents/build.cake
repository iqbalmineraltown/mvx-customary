/////////// INC /////////

#addin "nuget:?package=Cake.Json"
#addin "nuget:?package=Newtonsoft.Json&version=9.0.1"
// see T6493 regarding to Cake.Npm version
#addin "Cake.Npm"
#addin "Cake.StyleCop"
#addin "Cake.Powershell"
#tool "nuget:?package=xunit.runner.console"

var XMLModificationWarning = @"

<!--

                                   !!! WARNING !!!
                THIS FILE IS AUTO GENERATED. SEE AppConfigFiles FOLDER FOR SOURCE!
            FILE INI DIBUAT OTOMATIS. LIHAT FOLDER AppConfigFiles UNTUK SUMBER ASLINYA

-->

";

Action<string> FailInvalidSecret = (keyName) =>
{
  Information("");
  Information("");
  Information("");
  Information("                                  ========= P E R H A T I A N ========= ");
  Information("                               KUNCI SALAH, TIDAK SESUAI, ATAU TIDAK DITEMUKAN");
  Information("                      Mohon baca https://devs.peentar.com/w/development/mobile/secret/");
  Information("");
  Information("");
  Information("");
  throw new Exception(string.Format("Invalid secret for {0}. Please read the instruction message or visit https://devs.peentar.com/w/development/mobile/secret/", keyName));
};

Action FailGoogleServiceJson = () =>
{
  Information("");
  Information("");
  Information("");
  Information("                                  ========= P E R H A T I A N ========= ");
  Information("                               Berkas google-services.json TIDAK DITEMUKAN");
  Information("                      Mohon baca https://devs.peentar.com/w/development/mobile/secret/");
  Information("");
  Information("");
  Information("");
  throw new Exception(string.Format("google-services.json not found. Please read the instruction message or visit https://devs.peentar.com/w/development/mobile/secret/"));
};

Func<Dictionary<string, string>> RetrieveSecret = () =>
{
  if (!FileExists("./Secrets.json"))
  {
    CopyFile("./SecretsTemplate.json", "./Secrets.json");
  }

  var MandatorySecrets = new List<string>() {
    "GoogleMapsApiKey",
  };

  var RetrievedSecret = DeserializeJsonFromFile<Dictionary<string, string>>("./Secrets.json");

  foreach (var mandatoryKey in MandatorySecrets)
  {
    var envVarKey = string.Format("SECRET_{0}", mandatoryKey.ToUpper());
    if(configuration != "Debug") { // to avoid replacing debug key when dev on jenkin slave
      var envVarValue = EnvironmentVariable(envVarKey);
      if (envVarValue != null)
      {
        RetrievedSecret[mandatoryKey] = envVarValue;
      }
    }
    else if (!RetrievedSecret.ContainsKey(mandatoryKey))
    {
      FailInvalidSecret(mandatoryKey);
    }
  }

  return RetrievedSecret;
};

Func<string, string, string> RunCommand = (command, args) =>
{
  IEnumerable<string> stdout;
  var setting = new ProcessSettings() {
    RedirectStandardOutput = true,
    Arguments = args,
  };
  var res = StartProcess(command, setting, out stdout);
  if (res != 0)
  {
    throw new Exception("Failed to run command: " + command);
  }
  return stdout.First().Trim();
};

Action PrepareManifest = () =>
{
  var retrievedBuildConfig = appConfig.BuildConfigs[configuration];

  var BaseDir = "./" + appConfig.AndroidProjectFolder;

  var ConfigFilesDir = System.IO.Path.Combine(BaseDir, appConfig.AndroidAppConfigFilesFolder);

  CopyFile(System.IO.Path.Combine(ConfigFilesDir, retrievedBuildConfig.AndroidIconPath), appConfig.AndroidProjectFolder + "/Resources/drawable/Icon.png");

  var protectionString = retrievedBuildConfig.IsProtected ? "android:protectionLevel=\"signature\"" : "";

  TransformTextFile(System.IO.Path.Combine(ConfigFilesDir, "AndroidManifest.xml"), "{", "}")
    .WithToken("AndroidVersionCode", appConfig.AndroidVersionCode)
    .WithToken("AndroidVersionName", appConfig.AndroidVersionName)
    .WithToken("AndroidAppLabel", retrievedBuildConfig.AndroidAppName)
    .WithToken("AndroidPackageName", retrievedBuildConfig.AndroidPackageName)
    .WithToken("AndroidAppIcon", "@drawable/Icon")
    .WithToken("AndroidThemeName", $"@style/{retrievedBuildConfig.AndroidThemeName}")
    .WithToken("XMLModificationWarning", XMLModificationWarning)
    // .WithToken("ProtectionToken", protectionString)
    // .WithToken("GoogleMapsApiKey", RetrieveSecret()["GoogleMapsApiKey"])
    .Save(System.IO.Path.Combine(BaseDir, "Properties/AndroidManifest.xml"));
};

Action EnsureGoogleServicesJsonExist = () =>
{
	var filePath = "./" + appConfig.AndroidProjectFolder;
	if(configuration == "Debug"){
		filePath = filePath + "/google-services-dev.json";
	}
	else
	{
		filePath = filePath + "/google-services-prod.json";
	}
	if (!FileExists(filePath))
	{
		FailGoogleServiceJson();
	}
	else{
		CopyFile(filePath, appConfig.AndroidProjectFolder + "/google-services.json");
	}
};

public class AppConfig
{
  public string AndroidProjectFolder { get; set; }
  public string AndroidProjectFile { get; set; }
  public string AndroidAppConfigFilesFolder { get; set; }
  public int AndroidVersionCode { get; set; }
  public string AndroidVersionName { get; set; }
  public Dictionary<string, ConfigProps> BuildConfigs { get; set; }
  public string TestProjectFolder { get; set; }
  public string TestProjectFile { get; set; }
}

public class ConfigProps
{
  public string AndroidAppName { get; set; }
  public string AndroidPackageName { get; set; }
  public string AndroidIconPath { get; set; }
  public string AndroidThemeName { get; set; }
  public bool IsProtected { get; set; }
}

// one-stop retrieving project-specific configs
Func<AppConfig> RetrieveConfig = () =>
{
  var fileConfigPath = "./AppConfig.json";

  if (!FileExists(fileConfigPath))
  {
    throw new Exception(fileConfigPath + "not found. Deleted unintentionally, perhaps?");
  }

  var retrievedConfig = DeserializeJsonFromFile<AppConfig>(fileConfigPath);
  return retrievedConfig;
};

var appConfig = RetrieveConfig();
var solutionFile = GetFiles("./**/*.sln").First();
var androidProjPath = "./"+appConfig.AndroidProjectFolder+"/"+appConfig.AndroidProjectFile;

// we keep this global for .SetConfiguration command
var configuration = Argument("configuration", "Debug");

// this used on ForceClean
Action<Action> IgnoreException = (action) =>
{
  try
  {
    action();
  }
  catch (Exception ex)
  {
    Console.WriteLine("Exception, but was ignored.");
  }
};

/////////// END INC /////////

Task("Clean")
  .Does(() =>
{
  MSBuild(androidProjPath, new MSBuildSettings()
    .WithTarget("Clean")
    .WithProperty("AndroidSdkDirectory", EnvironmentVariable("ANDROID_HOME"))
    .SetConfiguration(configuration));
});

Task("ForceClean")
  .Does(() =>
{
  var delDirSettings = new DeleteDirectorySettings {
    Recursive = true,
    Force = true
  };

  IgnoreException(() => DeleteDirectory(appConfig.AndroidProjectFolder+"/bin", delDirSettings));
  IgnoreException(() => DeleteDirectory(appConfig.AndroidProjectFolder+"/obj", delDirSettings));
  IgnoreException(() => DeleteDirectory(".vs", delDirSettings));
});

Task("Setup")
  .Does(() =>
{
  PrepareManifest();
	// EnsureGoogleServicesJsonExist();
});

Task("SetupDev")
  .IsDependentOn("ForceClean")
  .IsDependentOn("Setup")
  .Does(() =>
{
  // Build for the first time to refresh Resources.Designer.cs
  MSBuild(androidProjPath, new MSBuildSettings().WithRestore());
});

Task("SetupPublish")
  .IsDependentOn("Setup")
  .Does(() =>
{

});

Task("Build")
  .Does(() =>
{
  MSBuild(androidProjPath, new MSBuildSettings()
    .WithTarget("SignAndroidPackage")
    .WithProperty("AndroidSdkDirectory", EnvironmentVariable("ANDROID_HOME"))
    .SetConfiguration(configuration));
  Console.WriteLine("BuildConfig: " + configuration);
});

Task("TfUpload")
  .Does(() =>
{
  var ApiKey = EnvironmentVariable("TF_API_KEY");
  var KeystorePath = EnvironmentVariable("TF_KEYSTORE");
  var StorePass = EnvironmentVariable("TF_STOREPASS");
  var Alias = EnvironmentVariable("TF_ALIAS");
  // there should 2 APKs, we want to take the 1st
  var ApkPath = GetFiles(appConfig.AndroidProjectFolder + "/bin/" +
    configuration + "/*.apk").First();

  Information("Will process: " + ApkPath);

  NpmInstall(new NpmInstallSettings().AddPackage("testfairy-uploader"));

  var args = "--api-key=" + ApiKey + " --keystore=" + KeystorePath + " --storepass=" + StorePass + " --alias=" + Alias + " " + ApkPath;
  var res = StartProcess("node_modules/testfairy-uploader/bin/testfairy-uploader.bat", args);
  if (res != 0)
  {
    throw new Exception("Failed to upload to TF");
  }
});

Task("MakeBuildInfo")
  .Does(() =>
{
  var tip = RunCommand("git", @"log --pretty=format:""%h""");
  var tipTime = RunCommand("git", @"log --pretty=format:""%aD""");
  var buildTime = DateTime.Now.ToString();

  Information("Generating build info:");
  Information(tip);
  Information(tipTime);
  Information(buildTime);

  var path = System.IO.Path.Combine(".", "RiverVm/RiverVm/BuildInfo.cs");

  TransformTextFile(path, "{", "}")
    .WithToken("GitTipTime", tipTime)
    .WithToken("GitTip", tip)
    .WithToken("BuildTime", buildTime)
    .Save(path);
});

Task("CiBuild")
  .IsDependentOn("SetupPublish")
  .IsDependentOn("Clean")
  .IsDependentOn("MakeBuildInfo")
  .IsDependentOn("Build")
  .Does(() =>
{
});

Action RunStyleCop = () =>
{
  var settingsFile = File("Settings.StyleCop");
  var resultFile = "./StyleReports/Result.xml";
  var htmlFile = "./StyleReports/Result.html";

  StyleCopAnalyse(settings => settings
    .WithSolution(solutionFile)
    .WithSettings(settingsFile)
    .ToResultFile(resultFile)
    .ToHtmlReport(htmlFile));
};

Task("StyleCop")
  .Does(() =>
{
  IgnoreException(() => DeleteDirectory("./StyleReports", true));
  CreateDirectory("./StyleReports");

  IgnoreException(RunStyleCop);
});

Task("CiTest")
  .IsDependentOn("StyleCop")
  .Does(() =>
{
  var testSettings = new XUnit2Settings()
  {
    HtmlReport = true,
    XmlReport = true,
    OutputDirectory = "./Reports",
  };

  var testDir = "./" + appConfig.TestProjectFolder;
  var testProjectFile = System.IO.Path.Combine(testDir, appConfig.TestProjectFile);

  MSBuild(testProjectFile, new MSBuildSettings()
    .WithTarget("Clean"));
  MSBuild(testProjectFile);

  IgnoreException(() => DeleteDirectory("./Reports", true));
  CreateDirectory("./Reports");

  XUnit2($"{testDir}/bin/Debug/{appConfig.TestProjectFolder}.dll", testSettings);
});

Task("Test")
  .IsDependentOn("StyleCop")
  .IsDependentOn("CiTest")
  .Does(() =>
{
});

Task("TfJenkins")
  .IsDependentOn("CiBuild")
  .IsDependentOn("CiTest")
  .IsDependentOn("TfUpload")
  .Does(() =>
{
});

Task("CiReleaseAndroid")
  .IsDependentOn("CiBuild")
  .IsDependentOn("CiTest")
  .Does(() =>
{
  var androidBuildToolsPath = EnvironmentVariable("ANDROID_HOME") + "\\build-tools";
  // Get latest available android build tools version
  var toolVer = GetSubDirectories(androidBuildToolsPath).Last();
  Information("Using build tools ver. " + toolVer);

  var apkPath = appConfig.AndroidProjectFolder + "/bin/" + configuration + "/";
  var packageName = appConfig.BuildConfigs[configuration].AndroidPackageName;
  Information("Will process: " + apkPath + packageName);

  // https://developer.xamarin.com/guides/android/deployment,_testing,_and_metrics/publishing/signing-the-app-package/manually-signing-the-apk/
  // zipalign (PS)
  StartPowershellScript($"{toolVer}/zipalign.exe -f -v 4 {apkPath}{packageName}.apk {apkPath}{packageName}-Aligned.apk");
  // apksigner (PS) -> Release Signed
  StartPowershellScript($"{toolVer}/apksigner.bat sign -v "
    + $"--ks {EnvironmentVariable("TF_KEYSTORE")} "
    + $"--ks-key-alias {EnvironmentVariable("TF_ALIAS")} "
    + "--ks-pass env:TF_STOREPASS "
    + $"{apkPath}{packageName}-Aligned.apk");

  Information("");
  Information("");
  Information("             ====== READY FOR PRODUCTION ====== ");
  Information("                        Well Done! :D");
  Information("");
  Information("");
});

Task("Default")
  .IsDependentOn("SetupDev")
  .Does(() =>
{
  Information("");
  Information("");
  Information("                ====== SETUP DONE ====== ");
  Information("          Welcome to Peentar Mobile Apps Team");
  Information("                       Have fun!");
  Information("");
  Information("");
});

var target = Argument("target", "Default");
RunTarget(target);