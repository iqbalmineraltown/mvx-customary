﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Xbp.Android.Extensions
{
    public static class ActivityExtension
    {
        public static Task<T> WaitOnUiThread<T>(this Activity act, Func<T> f)
        {
            TaskCompletionSource<T> tcs = new TaskCompletionSource<T>();
            act.RunOnUiThread(() =>
            {
                T result = default(T);
                try
                {
                    result = f();
                }
                catch (Exception)
                {
                    // Ignore it. But it should be impossible for now
                }
                tcs.SetResult(result);
            });
            return tcs.Task;
        }
    }
}
