﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Xbp.Android.Widgets
{
    /// <summary>
    /// Bindable Swipe Refresh Layout
    /// </summary>
    [Register("xbp.android.widgets.MvxSwipeRefreshLayout")]
    public class MvxSwipeRefreshLayout : SwipeRefreshLayout
    {
        public MvxSwipeRefreshLayout(Context context) : base(context)
        {
        }

        public MvxSwipeRefreshLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        protected MvxSwipeRefreshLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        private ICommand _refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand;
            }
            set
            {
                _refreshCommand = value;
                Refresh += RefreshCommand_Handler;
            }
        }

        private void RefreshCommand_Handler(object sender, EventArgs e)
        {
            var command = RefreshCommand;
            if (command == null)
                return;

            if (command.CanExecute(null))
                command.Execute(null);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Refresh -= RefreshCommand_Handler;
            }

            base.Dispose(disposing);
        }
    }
}
