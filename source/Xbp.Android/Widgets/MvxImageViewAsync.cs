﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using FFImageLoading;

namespace Xbp.Android.Widgets
{
    [Register("xbp.android.widgets.MvxImageViewAsync")]
    public class MvxImageViewAsync : FFImageLoading.Views.ImageViewAsync
    {
        public MvxImageViewAsync(Context context) : base(context)
        {
        }

        public MvxImageViewAsync(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MvxImageViewAsync(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MvxImageViewAsync(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
        }

        private string _imageUrl;
        public string ImageUrl
        {
            get { return _imageUrl; }
            set
            {
                _imageUrl = value;
                RefreshImage();
            }
        }

        private bool _initialized;

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            _initialized = true;
            if (ImageUrl != null)
            {
                RefreshImage();
            }
        }

        private void RefreshImage()
        {
            if (!_initialized)
            {
                return;
            }
            if (ImageUrl == null)
            {
                return;
            }

            FFImageLoading.Work.TaskParameter taskParam;
            if (ImageUrl.ToUpperInvariant().StartsWith("HTTP"))
            {
                taskParam = ImageService.Instance.LoadUrl(ImageUrl);
            }
            else
            {
                taskParam = ImageService.Instance.LoadFile(ImageUrl);
            }

            taskParam
                .LoadingPlaceholder("imageplaceholder.png", FFImageLoading.Work.ImageSource.ApplicationBundle)
                .ErrorPlaceholder("imageplaceholder.png", FFImageLoading.Work.ImageSource.ApplicationBundle)
                .Error(Util.ImageLoadingErrorHandler.HandleError)
                .Into(this);
        }
    }
}
