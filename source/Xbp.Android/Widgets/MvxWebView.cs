﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace Xbp.Android.Widgets
{
    /// <summary>
    /// Custom Webview which could retrieve/send Url from/to ViewModel
    /// </summary>
    [Register("xbp.android.widgets.MvxWebView")]
    public class MvxWebView : WebView
    {
        public MvxWebView(Context context) : base(context)
        {
            Initialize();
        }

        public MvxWebView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize();
        }

        public MvxWebView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Initialize();
        }

        public MvxWebView(Context context, IAttributeSet attrs, int defStyleAttr, bool privateBrowsing) : base(context, attrs, defStyleAttr, privateBrowsing)
        {
            Initialize();
        }

        public MvxWebView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Initialize();
        }

        protected MvxWebView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Initialize();
        }

        public void Initialize()
        {
            //we need custom WebViewClient so it wont try to open device browser when LoadUrl executed
            SetWebViewClient(new PeentarWebViewClient(this));
            Settings.JavaScriptEnabled = true;
        }

        //command to pass Url to ViewModel when page finish loaded
        public ICommand UrlFinishLoad { get; set; }

        private string _loadedUrl;
        public string LoadedUrl
        {
            get { return _loadedUrl; }
            set
            {
                _loadedUrl = value;

                if (UrlFinishLoad == null) return;
                if (!UrlFinishLoad.CanExecute(LoadedUrl)) return;
                UrlFinishLoad.Execute(LoadedUrl);
            }
        }

        private string _pageUrl;
        public string PageUrl
        {
            get { return _pageUrl; }
            set
            {
                _pageUrl = value;
                if (!string.IsNullOrEmpty(PageUrl))
                {
                    LoadUrl(PageUrl); // initial url to load
                }
            }
        }
    }

    internal class PeentarWebViewClient : WebViewClient
    {
        private MvxWebView _mvxWebView;

        public PeentarWebViewClient(MvxWebView mvxWebView)
        {
            _mvxWebView = mvxWebView;
        }

        public override void OnPageFinished(WebView view, string url)
        {
            base.OnPageFinished(view, url);

            if (!string.IsNullOrEmpty(url))
            {
                _mvxWebView.LoadedUrl = url;
            }
        }
    }
}
