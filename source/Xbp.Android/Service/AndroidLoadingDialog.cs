﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xbp.Service;

namespace Xbp.Android.Service
{
    public class AndroidLoadingDialog : ILoadingDialog
    {
        // AlertDialog instance is referenced to Activity, so it is fine
        // to wrap this inside WeakReference
        private WeakReference Instance { get; set; }

        public AndroidLoadingDialog(AlertDialog instance)
        {
            Instance = new WeakReference(instance);
        }

        public void Close()
        {
            ((AlertDialog)Instance.Target).Dismiss();
        }
    }
}
