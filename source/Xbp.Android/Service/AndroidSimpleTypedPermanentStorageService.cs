﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xbp.Service;

namespace Xbp.Android.Service
{
    public class AndroidSimpleTypedPermanentStorageService : ISimpleTypedPermanentStorageService
    {
        private Context CurrentContext
        {
            get
            {
                return Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity;
            }
        }

        private string GetDefaultKeyOf<T>()
        {
            return $"{Xbp.Util.SerializationUtil.GetFullNameOf<T>()}_Default";
        }

        public void Store<T>(T data)
        {
            var key = GetDefaultKeyOf<T>();
            Store<T>(key, data);
        }

        public void Store<T>(string key, T data)
        {
            var json = Xbp.Util.Json.PeentarJsonConverter.SerializeObject(data);

            var pref = PreferenceManager.GetDefaultSharedPreferences(CurrentContext).Edit();
            pref.PutString(key, json);
            pref.Commit();
        }

        public T Retrieve<T>()
        {
            // List is not supported for now, sorry.
            // Use strong key instead if you want
            var key = GetDefaultKeyOf<T>();
            return Retrieve<T>(key);
        }

        public T Retrieve<T>(string key)
        {
            var json = PreferenceManager.GetDefaultSharedPreferences(CurrentContext).GetString(key, null);

            if (json == null)
            {
                return default(T);
            }

            return Xbp.Util.Json.PeentarJsonConverter.DeserializeObject<T>(json);
        }

        public void Destroy<T>()
        {
            var key = GetDefaultKeyOf<T>();
            Destroy<T>(key);
        }

        public void Destroy<T>(string key)
        {
            var pref = PreferenceManager.GetDefaultSharedPreferences(CurrentContext).Edit();
            pref.Remove(key);
            pref.Commit();
        }
    }
}
