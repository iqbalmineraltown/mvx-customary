﻿using Android.Graphics;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using MvvmCross.UI;
using Xbp.Android.Extensions;
using Xbp.Service;
using System;
using System.Threading.Tasks;

namespace Xbp.Android.Service
{
    public class AndroidDialogService : IDialogService
    {
        private TaskCompletionSource<ConfirmationResultEnum> _confirmationTcs;

        protected global::Android.App.Activity CurrentActivity
        {
            get { return Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity; }
        }

        /// <summary>
        /// Show Alert Dialog.
        /// </summary>
        /// <param name="title">Dialog Title</param>
        /// <param name="text">Dialog Message</param>
        /// <param name="positiveButton">Positive Button Text</param>
        /// <param name="positiveAction">Positive Action</param>
        /// <param name="negativeButton">Negative Button Text</param>
        /// <param name="negativeAction">Negative Action</param>
        /// <param name="cancelable">Cancelable</param>
        public void ShowAlertDialog(string title, string message, string positiveButton = "OK", Action positiveAction = null,
            string negativeButton = "Cancel", Action negativeAction = null, bool cancelable = true)
        {
            var dialogBuilder = new AlertDialog.Builder(CurrentActivity);

            dialogBuilder.SetTitle(title);
            dialogBuilder.SetMessage(message);

            dialogBuilder.SetPositiveButton(positiveButton, (_, __) => positiveAction?.Invoke());
            if (negativeAction != null)
            {
                dialogBuilder.SetNegativeButton(negativeButton, (_, __) => negativeAction.Invoke());
            }
            dialogBuilder.SetCancelable(cancelable);

            CurrentActivity.RunOnUiThread(() => dialogBuilder.Show());
        }

        /// <summary>
        /// Show Confirmation dialog with yes & no button.
        /// </summary>
        /// <returns></returns>
        public Task<ConfirmationResultEnum> ShowConfirmationDialog(string title, string message, string positiveButton = "Yes",
            string negativeButton = "No")
        {
            _confirmationTcs = new TaskCompletionSource<ConfirmationResultEnum>();

            ShowAlertDialog(title, message, positiveButton,
                () =>
                {
                    _confirmationTcs?.SetResult(ConfirmationResultEnum.Affirmative);
                    _confirmationTcs = null;
                },
                negativeButton,
                () =>
                {
                    _confirmationTcs?.SetResult(ConfirmationResultEnum.Negative);
                    _confirmationTcs = null;
                },
                false);

            return _confirmationTcs.Task;
        }

        /// <summary>
        /// Show blocking loading dialog.
        /// Since ProgressDialog is deprecated in API 26, need to create custom progress dialog.
        /// Edit this method as needed.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public async Task<ILoadingDialog> ShowLoadingDialog(string message, string title = "Loading...")
        {
            // Need to put inside wrapper since different thread can't
            // change variable reference
            var dialog = await CurrentActivity
                .WaitOnUiThread(() =>
                    global::Android.App.ProgressDialog
                        .Show(CurrentActivity, title, message, true, false));

            return new AndroidLoadingDialog(dialog);
        }

        public void CloseLoadingDialog(ILoadingDialog dialog)
        {
            var d = dialog as AndroidLoadingDialog;
            CurrentActivity.RunOnUiThread(() => d?.Close());
        }

        public void ShowSnackBar(string message, SnackbarLengthEnum length = SnackbarLengthEnum.Short, MvxColor messageTextColor = null,
            string actionText = null, Action action = null, MvxColor actionTextColor = null,
            MvxColor snackbarBackgroundColor = null)
        {
            int snackBarLength;
            switch (length)
            {
                case SnackbarLengthEnum.Long:
                    snackBarLength = Snackbar.LengthLong;
                    break;

                case SnackbarLengthEnum.Indefinite:
                    snackBarLength = Snackbar.LengthIndefinite;
                    break;

                case SnackbarLengthEnum.Short:
                default:
                    snackBarLength = Snackbar.LengthShort;
                    break;
            }
            var parentView = ((ViewGroup)CurrentActivity.Window.DecorView.RootView
                .FindViewById(global::Android.Resource.Id.Content)).GetChildAt(0);//RootView of currently set XML of CurrentActivity
            var snackbar = Snackbar.Make(parentView, message, snackBarLength);
            if (messageTextColor != null)
            {
                var snackbarView = snackbar.View;
                var snackbarText = snackbarView.FindViewById<TextView>(MvvmCross.Droid.Support.V7.AppCompat.Resource.Id.snackbar_text);
                snackbarText.SetTextColor(new Color(messageTextColor.R, messageTextColor.G, messageTextColor.B, messageTextColor.A));
                if (snackbarBackgroundColor != null)
                {
                    snackbarView.SetBackgroundColor(new Color(snackbarBackgroundColor.R, snackbarBackgroundColor.G,
                        snackbarBackgroundColor.B, snackbarBackgroundColor.A));
                }
            }
            if (action != null && !string.IsNullOrEmpty(actionText))
            {
                snackbar.SetAction(actionText, x => action.Invoke());
                if (actionTextColor != null)
                {
                    snackbar.SetActionTextColor(new Color(actionTextColor.R, actionTextColor.G, actionTextColor.B, actionTextColor.A));
                }
            }
            CurrentActivity.RunOnUiThread(() => snackbar.Show());
        }

        public void ShowToast(string message, ToastLengthEnum length = ToastLengthEnum.Long)
        {
            var toastLength = length == ToastLengthEnum.Long ? ToastLength.Long : ToastLength.Short;
            CurrentActivity.RunOnUiThread(() => Toast.MakeText(CurrentActivity, message, toastLength).Show());
        }
    }
}
