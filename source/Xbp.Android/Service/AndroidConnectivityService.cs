﻿using Android.App;
using Android.Content;
using Android.Net;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xbp.Service;

namespace Xbp.Android.Service
{
    public class AndroidConnectivityService : IConnectivityService
    {
        protected Activity CurrentActivity
        {
            get { return Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity; }
        }

        public bool IsConnected()
        {
            var manager = (ConnectivityManager)CurrentActivity.GetSystemService(Context.ConnectivityService);
            var networkInfo = manager.ActiveNetworkInfo;
            return networkInfo != null && networkInfo.IsConnected;
        }
    }
}
