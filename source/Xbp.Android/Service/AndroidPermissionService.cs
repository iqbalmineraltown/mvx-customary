﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xbp.Service;

namespace Xbp.Android.Service
{
    /// <summary>
    /// This is based on james montemagno's permissions plugin
    /// https://github.com/jamesmontemagno/PermissionsPlugin/blob/master/src/Plugin.Permissions.Android/PermissionsImplementation.cs
    /// </summary>
    public class AndroidPermissionService : IPermissionService
    {
        private IList<string> _requestedPermission;
        public IList<string> RequestedPermission
        {
            get { return _requestedPermission; }
            set { _requestedPermission = value; }
        }

        private TaskCompletionSource<Dictionary<PermissionName, PermissionStatus>> _tcs;
        private object _lock = new Object();
        private Dictionary<PermissionName, PermissionStatus> _results;

        private Activity CurrentActivity
        {
            get { return Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity; }
        }

        public Task<PermissionStatus> CheckPermissionStatus(PermissionName permission)
        {
            var names = GetManifestNames(permission);

            if (names == null)
            {
                return Task.FromResult(PermissionStatus.Granted);
            }

            foreach (var name in names)
            {
                if (ContextCompat.CheckSelfPermission(CurrentActivity, name) == global::Android.Content.PM.Permission.Denied)
                    return Task.FromResult(PermissionStatus.Denied);
            }
            return Task.FromResult(PermissionStatus.Granted);
        }

        public async Task<Dictionary<PermissionName, PermissionStatus>> RequestPermissions(params PermissionName[] permissions)
        {
            if (_tcs != null && !_tcs.Task.IsCompleted)
            {
                _tcs.SetCanceled();
                _tcs = null;
            }
            lock (_lock)
            {
                _results = new Dictionary<PermissionName, PermissionStatus>();
            }
            var permissionsToRequest = new List<string>();
            foreach (var permission in permissions)
            {
                var result = await CheckPermissionStatus(permission).ConfigureAwait(false);
                if (result != PermissionStatus.Granted)
                {
                    var names = GetManifestNames(permission);
                    if ((names?.Count ?? 0) == 0)
                    {
                        _results.Add(permission, PermissionStatus.Unknown);
                        continue;
                    }

                    permissionsToRequest.AddRange(names);
                }
                else
                {
                    lock (_lock)
                    {
                        _results.Add(permission, PermissionStatus.Granted);
                    }
                }
            }

            if (permissionsToRequest.Count == 0)
            {
                return _results;
            }

            _tcs = new TaskCompletionSource<Dictionary<PermissionName, PermissionStatus>>();

            ActivityCompat.RequestPermissions(CurrentActivity, permissionsToRequest.ToArray(), PERMISSION_REQUEST_CODE);

            return await _tcs.Task.ConfigureAwait(false);
        }

        public Task<bool> ShouldShowRequestPermissionRationale(PermissionName permission)
        {
            var names = GetManifestNames(permission);

            if (names == null)
            {
                return Task.FromResult(false);
            }

            foreach (var name in names)
            {
                if (ActivityCompat.ShouldShowRequestPermissionRationale(CurrentActivity, name))
                {
                    return Task.FromResult(true);
                }
            }
            return Task.FromResult(false);
        }

        private const int PERMISSION_REQUEST_CODE = 31;

        public void OnRequestPermissionsResult(int requestCode, string[] permissions, global::Android.Content.PM.Permission[] grantResults)
        {
            if (requestCode != PERMISSION_REQUEST_CODE)
            {
                return;
            }

            if (_tcs == null)
            {
                return;
            }

            for (var i = 0; i < permissions.Length; i++)
            {
                if (_tcs.Task.Status == TaskStatus.Canceled)
                {
                    return;
                }

                var permission = GetPermissionName(permissions[i]);
                if (permission == PermissionName.Unknown)
                    continue;

                lock (_lock)
                {
                    if (!_results.ContainsKey(permission))
                    {
                        _results.Add(permission, grantResults[i] == global::Android.Content.PM.Permission.Granted ? PermissionStatus.Granted : PermissionStatus.Denied);
                    }
                }
            }
            _tcs.SetResult(_results);
        }

        private PermissionName GetPermissionName(string permission)
        {
            switch (permission)
            {
                case Manifest.Permission.Camera:
                    return PermissionName.Camera;

                case Manifest.Permission.ReadExternalStorage:
                case Manifest.Permission.WriteExternalStorage:
                    return PermissionName.Storage;

                case Manifest.Permission.AccessCoarseLocation:
                case Manifest.Permission.AccessFineLocation:
                    return PermissionName.Location;

                case Manifest.Permission.ReceiveSms:
                    return PermissionName.Sms;
            }
            return PermissionName.Unknown;
        }

        private List<string> GetManifestNames(PermissionName permission)
        {
            var permissionNames = new List<string>();
            switch (permission)
            {
                case PermissionName.Camera:
                    {
                        if (IsPermissionInManifest(Manifest.Permission.Camera))
                        {
                            permissionNames.Add(Manifest.Permission.Camera);
                        }
                    }
                    break;

                case PermissionName.Storage:
                    {
                        if (IsPermissionInManifest(Manifest.Permission.ReadExternalStorage))
                        {
                            permissionNames.Add(Manifest.Permission.ReadExternalStorage);
                        }

                        if (IsPermissionInManifest(Manifest.Permission.WriteExternalStorage))
                        {
                            permissionNames.Add(Manifest.Permission.WriteExternalStorage);
                        }
                    }
                    break;

                case PermissionName.Location:
                    {
                        if (IsPermissionInManifest(Manifest.Permission.AccessCoarseLocation))
                        {
                            permissionNames.Add(Manifest.Permission.AccessCoarseLocation);
                        }
                        if (IsPermissionInManifest(Manifest.Permission.AccessFineLocation))
                        {
                            permissionNames.Add(Manifest.Permission.AccessFineLocation);
                        }
                    }
                    break;

                case PermissionName.Sms:
                    {
                        if (IsPermissionInManifest(Manifest.Permission.ReceiveSms))
                        {
                            permissionNames.Add(Manifest.Permission.ReceiveSms);
                        }
                    }
                    break;

                default:
                    return null;
            }

            return permissionNames;
        }

        private bool IsPermissionInManifest(string permission)
        {
            try
            {
                if (RequestedPermission != null)
                {
                    return RequestedPermission.Any(requested => requested.Equals(permission, StringComparison.InvariantCultureIgnoreCase));
                }

                var info = CurrentActivity.PackageManager.GetPackageInfo(CurrentActivity.PackageName, global::Android.Content.PM.PackageInfoFlags.Permissions);

                if (info == null)
                {
                    return false;
                }

                RequestedPermission = info.RequestedPermissions;

                if (RequestedPermission == null)
                {
                    return false;
                }

                return RequestedPermission.Any(requested => requested.Equals(permission, StringComparison.InvariantCultureIgnoreCase));
            }
            catch
            {
            }
            return false;
        }
    }
}
