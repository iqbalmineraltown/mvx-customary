﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android;

namespace Xbp.Android.Util
{
    public static class AndroidUtils
    {
        public static string GetAppName(Context context)
        {
            var appInfo = context.ApplicationInfo;
            var stringId = appInfo.LabelRes;
            return stringId == 0 ? appInfo.NonLocalizedLabel.ToString() : context.GetString(stringId);
        }

        public static T GetFragmentOf<T>(MvvmCross.Droid.Support.V7.AppCompat.MvxAppCompatActivity activity) where T : class
        {
            return activity
                .SupportFragmentManager
                .Fragments
                .FirstOrDefault(x => x is T) as T;
        }
    }
}
