﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Xbp.Android.Util
{
    public static class ImageLoadingErrorHandler
    {
        public static void HandleError(Exception ex)
        {
            System.Diagnostics.Debug.WriteLine("Error when loading image: " + ex.Message);
        }
    }
}
