﻿using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.IoC;
using MvvmCross.Platforms.Android.Presenters;
using MvvmCross.Presenters;
using Xbp.Android.PresentationHandlers;
using Xbp.Core;
using Xbp.PresentationHint;
using Xbp.Util;
using System.Collections.Generic;

namespace Xbp.Android.Core
{
    public abstract class BaseMvxAndroidSetup<TApp> : MvxAppCompatSetup<TApp> where TApp : BaseMvxApplication, new()
    {
        /// <summary>
        /// List bottomsheet presentationhint & respective handler here.
        /// Give empty dictionary if no bottomsheet used
        /// </summary>
        protected virtual Dictionary<BasePresentationHint, BasePresentationHandler> CustomPresentations
            => new Dictionary<BasePresentationHint, BasePresentationHandler>();

        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();

            CreatableTypes(typeof(BaseMvxAndroidSetup<>).Assembly)
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            var viewPresenter = new PeentarMvxAndroidViewPresenter(AndroidViewAssemblies);
            var methodName = nameof(IMvxViewPresenter.AddPresentationHintHandler);

            foreach (var entry in CustomPresentations)
            {
                var handler = entry.Value.Handler;
                ReflectionHelper.InvokeGenericMethod<IMvxViewPresenter>(viewPresenter, methodName, entry.Key.GetType(), new[] { handler });
            }

            return viewPresenter;
        }
    }
}
