﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using Xbp.Core;

namespace Xbp.Android.Core
{
    public enum LayoutParamEnum
    {
        MatchParent = ViewGroup.LayoutParams.MatchParent,
        WrapContent = ViewGroup.LayoutParams.WrapContent,
    }

    public abstract class BaseMvxDialog : MvxDialogFragment
    {
        // assign layout
        protected abstract int LayoutId { get; }

        // enable title
        protected virtual bool HasTitle => false;

        // set title
        protected virtual string DialogTitle => "This is Dialog";

        // assumed default dialog size
        protected virtual LayoutParamEnum DialogWidth => LayoutParamEnum.MatchParent;

        protected virtual LayoutParamEnum DialogHeight => LayoutParamEnum.WrapContent;

        // inheriting class should implement constructor with base reference
        // as seen on https://github.com/MvvmCross/MvvmCross/blob/develop/TestProjects/Playground/Playground.Droid/Views/ModalView.cs
        public BaseMvxDialog() : base()
        {
        }

        public BaseMvxDialog(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(LayoutId, null, false);
            if (!HasTitle)
            {
                Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            }

            return view;
        }

        public override void OnStart()
        {
            base.OnStart();
            // viewgroup size still need to be set on OnStart()
            Dialog.Window.SetLayout((int)DialogWidth, (int)DialogHeight);
        }
    }

    public abstract class BaseMvxDialog<TViewModel>
        : BaseMvxDialog, IMvxFragmentView<TViewModel> where TViewModel : BaseViewModel
    {
        // inheriting class should implement constructor with base reference
        public BaseMvxDialog() : base()
        {
        }

        public BaseMvxDialog(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            ViewModel?.PostViewCreated();

            return view;
        }

        public new TViewModel ViewModel
        {
            get { return (TViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
    }
}
