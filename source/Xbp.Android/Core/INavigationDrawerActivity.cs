﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Xbp.Android.Core
{
    //To Handle Setup For Navigation Drawer
    public interface INavigationDrawerActivity
    {
        bool NavDrawerSetUp { get; }

        void SetNavDrawerEnabled(bool shouldEnable);

        void SetupNavigationDrawerToggler(global::Android.Support.V7.Widget.Toolbar toolbar, bool homeAsUp);

        void TearDownNavigationDrawerToggler();

        void SyncState();

        void CloseDrawer();
    }
}
