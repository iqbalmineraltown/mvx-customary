﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using Xbp.Core;
using Xbp.Android.Util;
using Xbp.Util;

namespace Xbp.Android.Core
{
    public abstract class BaseMvxFragment<TViewModel> : MvxFragment<TViewModel> where TViewModel : BaseViewModel
    {
        protected abstract int LayoutId { get; }

        protected virtual int ToolbarId { get; }

        protected virtual string FragmentTitle { get; }

        protected virtual bool HomeAsUp => true;

        /// <summary>
        /// To notify fragment to setup navdrawer toggler in parent activity
        /// </summary>
        protected virtual bool ShouldSetupNavDrawerToggler => false;

        /// <summary>
        /// Enable/disable nav drawer
        /// </summary>
        protected virtual bool ShouldEnableNavDrawer => false;

        private View _inflatedView;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            WillCreateView();

            var ignore = base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(LayoutId, null, false);

            ProcessToolbar(view);

            _inflatedView = ProcessView(view);

            ViewModel?.PostViewCreated();

            return _inflatedView;
        }

        protected virtual void WillCreateView()
        {
        }

        protected virtual void ProcessToolbar(View view)
        {
            var toolbar = view.FindViewById<global::Android.Support.V7.Widget.Toolbar>(ToolbarId);
            if (toolbar != null)
            {
                var title = FragmentTitle ?? AndroidUtils.GetAppName(Context);

                toolbar.Title = title;

                if (Activity is INavigationDrawerActivity navDrawerActivity)
                {
                    if (ShouldSetupNavDrawerToggler)
                    {
                        navDrawerActivity.SetupNavigationDrawerToggler(toolbar, HomeAsUp);
                    }
                    else
                    {
                        navDrawerActivity.TearDownNavigationDrawerToggler();
                    }

                    navDrawerActivity.SetNavDrawerEnabled(ShouldEnableNavDrawer);
                }
                else
                {
                    if (ShouldSetupNavDrawerToggler || ShouldEnableNavDrawer)
                    {
                        throw new ImpossibleStateException("Trying to setup navigation drawer on non INavigationDrawerActivity. If this is intended make sure parent activity implements INavigationDrawerActivity");
                    }
                }

                if (Activity is AppCompatActivity activity)
                {
                    activity.SetSupportActionBar(toolbar);
                    if (HomeAsUp)
                    {
                        activity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                    }
                }
                else
                {
                    throw new ImpossibleStateException("Couldn't set toolbar. Make sure your activity is AppCompatActivity");
                }
            }
            else if (!(view is global::Android.Support.Design.Widget.NavigationView)
                && (Activity is INavigationDrawerActivity navDrawerActivity))
            {
                navDrawerActivity.SetNavDrawerEnabled(ShouldEnableNavDrawer);
            }
        }

        protected abstract View ProcessView(View baseView);

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!HomeAsUp)
            {
                return base.OnOptionsItemSelected(item);
            }

            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    Activity.OnBackPressed();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override void OnDestroy()
        {
            _inflatedView?.Dispose();
            _inflatedView = null;

            base.OnDestroy();
        }
    }
}
