﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using Xbp.Core;
using Xbp.Service;
using Xbp.Android.Service;
using Xbp.Android.Util;
using Android.Content.PM;

namespace Xbp.Android.Core
{
    public abstract class BaseMvxActivity<TViewModel> : MvxAppCompatActivity<TViewModel> where TViewModel : BaseViewModel
    {
        protected abstract int LayoutId { get; }

        // assuming toolbar is not always implemented
        protected virtual int ToolbarId { get; }

        protected virtual string ActivityTitle { get; }

        // assuming back button available on toolbar by default
        protected virtual bool HomeAsUp => true;

        // default orientation is portrait
        protected virtual global::Android.Content.PM.ScreenOrientation ScreenOrientation => global::Android.Content.PM.ScreenOrientation.Portrait;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestedOrientation = ScreenOrientation;

            SetContentView(LayoutId);

            var toolbar = FindViewById<global::Android.Support.V7.Widget.Toolbar>(ToolbarId);
            if (toolbar != null)
            {
                toolbar.Title = ActivityTitle ?? AndroidUtils.GetAppName(this);
                SetSupportActionBar(toolbar);

                if (HomeAsUp)
                {
                    SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                }
            }

            ViewModel?.PostViewCreated();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!HomeAsUp)
            {
                return base.OnOptionsItemSelected(item);
            }

            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    base.OnBackPressed();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        //to handle result from requesting permissions
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            var permissionService = (AndroidPermissionService)MvvmCross.Mvx.Resolve<IPermissionService>();
            permissionService.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
