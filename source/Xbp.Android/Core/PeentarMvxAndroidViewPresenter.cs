﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;

namespace Xbp.Android.Core
{
    public class PeentarMvxAndroidViewPresenter : MvxAppCompatViewPresenter
    {
        public PeentarMvxAndroidViewPresenter(IEnumerable<Assembly> androidViewAssemblies) : base(androidViewAssemblies)
        {
        }

        protected override bool CloseFragment(IMvxViewModel viewModel, MvxFragmentPresentationAttribute attribute)
        {
            //Close Activity if backstack is empty
            if (CurrentFragmentManager.BackStackEntryCount == 0)
            {
                CurrentActivity.OnBackPressed();
                return true;
            }

            return base.CloseFragment(viewModel, attribute);
        }
    }
}
