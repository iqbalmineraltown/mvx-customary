﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xbp.PresentationHint;

namespace Xbp.Android.PresentationHandlers
{
    public abstract class BasePresentationHandler
    {
        public abstract Func<BasePresentationHint, bool> Handler { get; }
    }
}
