﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using Xbp.Backends;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Xbp.Core
{
    public class BaseMvxApplication : MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();

            CreatableTypes(typeof(BaseMvxApplication).Assembly)
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.LazyConstructAndRegisterSingleton<IHttpClientFactory>(() => new HttpClientFactory());
            Mvx.LazyConstructAndRegisterSingleton<IUriProvider>(() => new UriProvider());
        }
    }
}
