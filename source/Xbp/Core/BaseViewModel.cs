﻿using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xbp.Service;
using Xbp.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Xbp.Core
{
    public class BaseViewModel : MvxViewModel
    {
        private IMvxNavigationService _navService;
        protected IMvxNavigationService NavService
        {
            get
            {
                _navService = _navService ?? Mvx.Resolve<IMvxNavigationService>();
                return _navService;
            }
        }

        private IDialogService _dialogService;
        protected IDialogService DialogService
        {
            get
            {
                _dialogService = _dialogService ?? Mvx.Resolve<IDialogService>();
                return _dialogService;
            }
        }

        public async Task Navigate<TViewModel>() where TViewModel : BaseViewModel
        {
            await NavService.Navigate<TViewModel>();
        }

        public async Task Navigate<TViewModel, TParam>(TParam param)
            where TViewModel : BaseViewModel<TParam>
        {
            await NavService.Navigate<TViewModel, TParam>(param);
        }

        public async Task<TResult> Navigate<TViewModel, TResult>()
            where TViewModel : BaseViewModelResult<TResult>
        {
            var res = await NavService.Navigate<TViewModel, TResult>();
            return res;
        }

        public async Task<TResult> Navigate<TViewModel, TParam, TResult>(TParam param)
            where TViewModel : BaseViewModel<TParam, TResult>
        {
            var res = await NavService.Navigate<TViewModel, TParam, TResult>(param);
            return res;
        }

        public MvxNotifyTask RunAsync(Func<Task> function, Action<Exception> exceptionAction = null)
        {
            exceptionAction = exceptionAction ?? OnRunAsyncException;
            return RunAsyncInternal(function, false, null, exceptionAction);
        }

        public MvxNotifyTask RunAsync(Func<Task> function, bool allowConcurrent,
            string mutexName = null, Action<Exception> exceptionAction = null)
        {
            exceptionAction = exceptionAction ?? OnRunAsyncException;
            return RunAsyncInternal(function, allowConcurrent, mutexName, exceptionAction);
        }

        private static Dictionary<string, bool> _lockDictionary = new Dictionary<string, bool>();

        private MvxNotifyTask RunAsyncInternal(Func<Task> function, bool allowConcurrent,
            string mutexName, Action<Exception> exceptionAction)
        {
            return MvxNotifyTask.Create(async () =>
            {
                var mutex = mutexName ?? $"{GetType().FullName}.{function.GetMethodInfo().Name}";

                if (!allowConcurrent)
                {
                    lock (mutex)
                    {
                        if (!_lockDictionary.ContainsKey(mutex))
                        {
                            _lockDictionary.Add(mutex, false);
                        }
                        else if (_lockDictionary[mutex])
                        {
                            return;
                        }

                        _lockDictionary[mutex] = true;
                    }
                }

                try
                {
                    await function();
                }
                catch (Exception ex)
                {
                    exceptionAction(ex);
                }
                finally
                {
                    if (!allowConcurrent)
                    {
                        lock (mutex)
                        {
                            _lockDictionary[mutex] = false;
                        }
                    }
                }
            });
        }

        private void OnRunAsyncException(Exception ex)
        {
            PeentarLogger.DebugWriteLine($"Unhandled exception when doing async task: {ex}");
            PeentarLogger.DebugWriteLine($"Stack trace: {ex.StackTrace}");
            InvokeOnMainThread(() =>
            {
                throw ex;
            });
        }

        private CancellationTokenSource _cts { get; set; }

        public CancellationToken CancelToken
        {
            get
            {
                _cts = _cts ?? new CancellationTokenSource();
                return _cts.Token;
            }
        }

        /// <summary>
        /// Cancel Task, e.g. when user navigate when fetching data
        /// </summary>
        public void RequestCancellation()
        {
            /// should prevent from re-cancel for 2nd time
            /// its okay to request cancel after task finished
            if ((_cts?.IsCancellationRequested) == false)
            {
                _cts?.Cancel();
                _cts?.Dispose();
                _cts = null;
            }
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            RequestCancellation();

            base.ViewDestroy(viewFinishing);
        }

        public async Task CloseSelf()
        {
            await NavService.Close(this);
        }

        public async Task ChangePresentation(MvxPresentationHint hint)
        {
            await NavService.ChangePresentation(hint);
        }

        public virtual void PostViewCreated()
        {
        }
    }

    public abstract class BaseViewModel<TParam> : BaseViewModel, IMvxViewModel<TParam>
    {
        public abstract void Prepare(TParam param);
    }

    public class BaseViewModelResult<TResult> : BaseViewModel, IMvxViewModelResult<TResult>
    {
        public TaskCompletionSource<object> CloseCompletionSource { get; set; }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            if (CloseCompletionSource != null && !CloseCompletionSource.Task.IsCompleted && !CloseCompletionSource.Task.IsFaulted)
                CloseCompletionSource?.TrySetCanceled();
            base.ViewDestroy();
        }

        public async Task CloseSelf(TResult result)
        {
            await NavService.Close(this, result);
        }
    }

    public abstract class BaseViewModel<TParam, TResult> : BaseViewModelResult<TResult>, IMvxViewModel<TParam, TResult>
    {
        public abstract void Prepare(TParam parameter);
    }
}
