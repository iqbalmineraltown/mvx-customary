﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace Xbp.Util.Json
{
    /// <summary>
    /// Warning: Can't deserialize acronym. See the test.
    /// </summary>
    /// <see cref="https://gist.github.com/roryf/1042502"/>
    public class OmnichannelEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            var isNullableType = (objectType.GetTypeInfo().IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>));

            Type t = (isNullableType)
                ? Nullable.GetUnderlyingType(objectType)
                : objectType;

            return t.GetTypeInfo().IsEnum;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool isNullable = (objectType.GetTypeInfo().IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>));

            if (reader.TokenType == JsonToken.Null)
            {
                if (!isNullable)
                {
                    throw new ImpossibleStateException(string.Format("Cannot convert null value to {0}.", CultureInfo.InvariantCulture, objectType));
                }

                return null;
            }

            Type t = isNullable ? Nullable.GetUnderlyingType(objectType) : objectType;

            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    var converted = RetrieveDotNetEnumNameFromJava(reader.Value.ToString());
                    return Enum.Parse(t, converted);
                }
                if (reader.TokenType == JsonToken.Integer)
                {
                    new ImpossibleStateException(string.Format("Integer value {0} is not allowed.", CultureInfo.InvariantCulture, reader.Value));
                }
            }
            catch (Exception)
            {
                throw new ImpossibleStateException($"Error converting value {reader.Value} to type '{objectType}'.");
            }

            // we don't actually expect to get here.
            throw new ImpossibleStateException(string.Format("Unexpected token {0} when parsing enum.", CultureInfo.InvariantCulture, reader.TokenType));
        }

        protected string RetrieveDotNetEnumNameFromJava(string javaEnum)
        {
            var words = javaEnum.Split('_');
            var res = new List<string>();
            foreach (var word in words)
            {
                // Probably double underscore.
                if (word.Length == 0)
                {
                    throw new ImpossibleStateException("Don't know how to process double underscore!");
                }

                var builder = new StringBuilder();

                for (int i = 0; i < word.Length; i++)
                {
                    var c = word[i];
                    if (i == 0)
                    {
                        builder.Append(char.ToUpper(c));
                    }
                    else
                    {
                        builder.Append(char.ToLower(c));
                    }
                }

                res.Add(builder.ToString());
            }

            return string.Join("", res);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            var e = (Enum)value;
            var enumName = e.ToString("G");

            if (char.IsNumber(enumName[0]) || enumName[0] == '-')
            {
                // enum value has no name so write number
                writer.WriteValue(value);
            }
            else
            {
                var enumType = e.GetType();

                var words = new List<string>();
                var currentWord = new StringBuilder();

                if (char.IsLower(enumName[0]))
                {
                    throw new ImpossibleStateException("Don't know how to process non-conventional enum!");
                }

                foreach (var c in enumName)
                {
                    if (currentWord.Length > 0 && char.IsUpper(c))
                    {
                        words.Add(currentWord.ToString());
                        currentWord.Clear();
                    }
                    currentWord.Append(char.ToUpper(c));
                }

                if (currentWord.Length > 0)
                {
                    words.Add(currentWord.ToString());
                }

                writer.WriteValue(string.Join("_", words));
            }
        }
    }
}
