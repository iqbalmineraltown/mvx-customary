﻿using MvvmCross;
using MvvmCross.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Xbp.Util.Json
{
    public class PeentarJsonConverter
    {
        protected static JsonSerializerSettings CreateDecoratedSetting()
        {
            var setting = new JsonSerializerSettings()
            {
                /// Usually we use CamelCase/snake_case. You may change this as needed
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
            };

            /// These are common settings and converters used in our system
            setting.DateParseHandling = DateParseHandling.DateTimeOffset;

            setting.Converters.Add(new OmnichannelEnumConverter());
            setting.Converters.Add(new IsoDateTimeConverter());

            /// Add Customized Converter Here

            return setting;
        }

        private static JsonSerializerSettings _decoratedSettings;
        protected static JsonSerializerSettings DecoratedSettings
        {
            get
            {
                _decoratedSettings = _decoratedSettings ?? CreateDecoratedSetting();
                return _decoratedSettings;
            }
        }

        public static T DeserializeObject<T>(string inputText)
        {
            return JsonConvert.DeserializeObject<T>(inputText, DecoratedSettings);
        }

        public static string SerializeObject(object toSerialize)
        {
            return JsonConvert.SerializeObject(toSerialize, DecoratedSettings);
        }

        public static object DeserializeObject(Type type, string inputText)
        {
            return JsonConvert.DeserializeObject(inputText, type, DecoratedSettings);
        }
    }
}
