﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xbp.Util
{
    public static class ReflectionHelper
    {
        public static void InvokeGenericMethod<T>(T target, string methodName, Type genericType, object[] parameters)
        {
            var callerType = typeof(T);
            var method = callerType.GetMethod(methodName);
            var genericMethod = method.MakeGenericMethod(genericType);
            genericMethod.Invoke(target, parameters);
        }
    }
}
