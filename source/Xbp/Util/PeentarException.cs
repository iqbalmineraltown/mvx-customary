﻿using System;

/// <summary>
/// This file should contains all expected exceptions which may occurs on our system
/// </summary>

namespace Xbp.Util
{
    /// <summary>
    /// Base exception for our system-specific exceptions
    /// </summary>
    public class PeentarException : Exception
    {
        public PeentarException() : base()
        {
        }

        public PeentarException(string message) : base(message)
        {
        }

        public PeentarException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    /// <summary>
    /// Exception for impossible state. Usually due to business logic failure
    /// May not happened on production, but more likely on development
    /// </summary>
    public class ImpossibleStateException : PeentarException
    {
        public ImpossibleStateException(string message) : base(message)
        {
        }
    }

    public class NetworkProblemException : PeentarException
    {
        public string Uri { get; set; }
        public System.Net.Http.HttpResponseMessage Response { get; set; }

        public NetworkProblemException(string message, Backends.HttpRequestEnvelope envelope, Exception innerException)
            : base($"Error when requesting to {envelope.Request.RequestUri}: {message}", innerException)
        {
        }
    }

    public class BadRequestException : PeentarException
    {
    }

    public class UnauthorizedException : PeentarException
    {
    }

    public class ForbiddenException : PeentarException
    {
    }

    public class NotFoundException : PeentarException
    {
    }

    public class NonSuccessResponseException : NetworkProblemException
    {
        public NonSuccessResponseException(System.Net.HttpStatusCode StatusCode, Backends.HttpRequestEnvelope envelope)
            : base(StatusCode.ToString(), envelope, null)
        {
        }
    }

    public class NoConnectionAvailableException : PeentarException
    {
    }
}
