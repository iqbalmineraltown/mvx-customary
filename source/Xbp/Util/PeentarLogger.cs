﻿using MvvmCross;
using MvvmCross.Logging;

namespace Xbp.Util
{
    /// <summary>
    /// See https://www.mvvmcross.com/documentation/fundamentals/logging for further customization
    /// </summary>
    public class PeentarLogger
    {
        public static void DebugWriteLine(string message)
        {
            var logger = Mvx.Resolve<IMvxLog>();
            logger.Debug(message);
        }
    }
}
