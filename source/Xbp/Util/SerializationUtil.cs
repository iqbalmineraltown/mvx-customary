﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xbp.Util
{
    public class SerializationUtil
    {
        public static string GetFullNameOf<T>()
        {
            return typeof(T).FullName;
        }
    }
}
