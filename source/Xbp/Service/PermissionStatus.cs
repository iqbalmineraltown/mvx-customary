﻿namespace Xbp.Service
{
    /// <summary>
    /// reference:
    /// https://github.com/jamesmontemagno/PermissionsPlugin/blob/master/src/Plugin.Permissions.Abstractions/PermissionEnums.cs
    /// </summary>
    public enum PermissionStatus
    {
        Denied,
        Granted,
        Unknown
    }
}
