﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xbp.Service
{
    public interface IConnectivityService
    {
        bool IsConnected();
    }
}
