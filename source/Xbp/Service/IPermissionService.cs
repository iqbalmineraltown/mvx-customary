﻿using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xbp.Service
{
    /// <summary>
    /// reference:
    /// https://github.com/jamesmontemagno/PermissionsPlugin/blob/master/src/Plugin.Permissions.Abstractions/IPermissions.cs
    /// </summary>
    public interface IPermissionService
    {
        IList<string> RequestedPermission { get; set; }

        Task<PermissionStatus> CheckPermissionStatus(PermissionName permission);

        Task<bool> ShouldShowRequestPermissionRationale(PermissionName permission);

        Task<Dictionary<PermissionName, PermissionStatus>> RequestPermissions(params PermissionName[] permissions);
    }
}
