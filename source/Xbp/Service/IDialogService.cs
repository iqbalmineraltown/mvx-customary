﻿using MvvmCross.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Xbp.Service
{
    public interface IDialogService
    {
        void ShowAlertDialog(string title, string message
            , string positiveButton = "OK", Action positiveAction = null
            , string negativeButton = "Cancel", Action negativeAction = null
            , bool cancelable = true);

        Task<ConfirmationResultEnum> ShowConfirmationDialog(string title, string message,
            string positiveButton = "Yes", string negativeButton = "No");

        Task<ILoadingDialog> ShowLoadingDialog(string message, string title = "Loading...");

        void CloseLoadingDialog(ILoadingDialog dialog);

        void ShowSnackBar(string message, SnackbarLengthEnum length = SnackbarLengthEnum.Short, MvxColor messageTextColor = null,
            string actionText = null, Action action = null, MvxColor actionTextColor = null,
            MvxColor snackbarBackgroundColor = null);

        void ShowToast(string message, ToastLengthEnum length = ToastLengthEnum.Long);
    }

    public enum ConfirmationResultEnum
    {
        Affirmative,
        Negative,
    }

    public enum SnackbarLengthEnum
    {
        Short,
        Long,
        Indefinite
    }

    public enum ToastLengthEnum
    {
        Short,
        Long
    }
}
