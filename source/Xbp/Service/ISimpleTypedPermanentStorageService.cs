﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xbp.Service
{
    public interface ISimpleTypedPermanentStorageService
    {
        void Store<T>(T data);

        void Store<T>(string key, T data);

        T Retrieve<T>();

        T Retrieve<T>(string key);

        void Destroy<T>();

        void Destroy<T>(string key);
    }
}
