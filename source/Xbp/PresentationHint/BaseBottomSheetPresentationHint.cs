﻿using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xbp.PresentationHint
{
    public abstract class BaseBottomSheetPresentationHint : BasePresentationHint
    {
        public BaseBottomSheetPresentationHint(bool isExpanded)
        {
            IsExpanded = isExpanded;
        }

        public abstract bool IsExpanded { get; set; }
    }
}
