﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Xbp.Backends
{
    /// <summary>
    /// Wrap response with additional information
    /// </summary>
    public class HttpRequestEnvelope : IDisposable
    {
        private static long LatestRequestId { get; set; }
        public long RequestId { get; private set; }
        private HttpClient _client;
        private HttpRequestMessage _request;
        private Stopwatch _stopWatch;
        public HttpRequestMessage Request => _request;

        public HttpRequestEnvelope(HttpClient htc, HttpRequestMessage req)
        {
            _client = htc;
            _request = req;
            RequestId = LatestRequestId++;
        }

        public Task<HttpResponseMessage> SendAsync(CancellationToken ct)
        {
            _stopWatch = new Stopwatch();
            _stopWatch.Start();
            return _client.SendAsync(_request, ct);
        }

        public long ElapsedMs => _stopWatch.ElapsedMilliseconds;

        public void Dispose()
        {
            _client = null;

            _request.Dispose();
            _request = null;

            _stopWatch.Stop();
            _stopWatch.Reset();
            _stopWatch = null;
        }
    }
}
