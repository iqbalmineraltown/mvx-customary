﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Xbp.Backends
{
    public interface IHttpClientFactory
    {
        HttpClient CreateHttpClient(int TimeoutSecond = 10);
    }

    public class HttpClientFactory : IHttpClientFactory
    {
        /// <summary>
        /// Create http client object
        /// </summary>
        /// <param name="TimeoutSecond">set timeout duration in seconds</param>
        /// <returns>Http Client Object</returns>
        public HttpClient CreateHttpClient(int TimeoutSecond = 10)
        {
            var htc = new HttpClient
            {
                Timeout = new TimeSpan(0, 0, TimeoutSecond)
            };
            return htc;
        }
    }
}
