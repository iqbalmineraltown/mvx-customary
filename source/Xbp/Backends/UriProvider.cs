﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Xbp.Backends
{
    /// <summary>
    /// Providing base uri for each build configuration/environment
    /// </summary>
    public class UriProvider : IUriProvider
    {
        /// <summary>
        /// Write base url without ending slash
        /// </summary>

        private string _baseUrl;
        public string BaseUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_baseUrl))
                {
                    throw new NotImplementedException("BaseUrl is not specified");
                }
                return _baseUrl;
            }
            set
            {
                _baseUrl = value;
            }
        }

        private string _baseResourceUrl;
        public string BaseResourceUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_baseResourceUrl))
                {
                    throw new NotImplementedException("BaseResourceUrl is not specified");
                }
                return _baseUrl;
            }
            set
            {
                _baseUrl = value;
            }
        }

        // this need to be separated from BaseUrl because we need to get absolute Uri at some point
        // write this url starting with slash and without ending slash
        public string BaseVersion { get; set; }

        public string Provide(string endpoint)
        {
            var timeStamp = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return $"{BaseUrl}{endpoint}?_timestamp={timeStamp}";
        }

        public string Provide(string endpoint, IEnumerable<KeyValuePair<string, string>> queryStrings)
        {
            // Need some hacks since we don't get HttpUtility on mono profile
            // See: http://stackoverflow.com/questions/17096201/build-query-string-for-system-net-httpclient-get
            var timeStamp = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            var timeStampQuery = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("_timestamp", timeStamp.ToString()) };
            var realQueryString = queryStrings.Concat(timeStampQuery);

            var queryStringProcessor = new FormUrlEncodedContent(realQueryString).ReadAsStringAsync();
            queryStringProcessor.Wait();

            var encodedQueryString = queryStringProcessor.Result;
            return $"{BaseUrl}{endpoint}?{encodedQueryString}";
        }

        public string ProvideStaticResourceUrl(string filePath)
        {
            return $"{BaseResourceUrl}{filePath}";
        }
    }

    public interface IUriProvider
    {
        string BaseUrl { get; set; }
        string BaseResourceUrl { get; set; }
        string BaseVersion { get; set; }

        string Provide(string endpoint);

        string Provide(string endpoint, IEnumerable<KeyValuePair<string, string>> queryStrings);

        string ProvideStaticResourceUrl(string filePath);
    }
}
