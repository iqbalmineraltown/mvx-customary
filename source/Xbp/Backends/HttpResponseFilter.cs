﻿using Xbp.Util;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Xbp.Backends
{
    public static class HttpResponseFilter
    {
        /// <summary>
        /// Check HTTP status and throw general exception message
        /// </summary>
        /// <param name="prev">
        ///     The response message to be filtered
        /// </param>
        /// <returns></returns>
        public static async Task<string> AwaitStringContent(this HttpRequestEnvelope prev, CancellationToken ct = default(CancellationToken))
        {
            try
            {
                var returnTask = await AwaitStringContentInternal(prev, ct);
                PeentarLogger.DebugWriteLine($"[{prev.RequestId}]  {prev.Request.Method} {prev.Request.RequestUri} took {prev.ElapsedMs} ms.");
                prev.Dispose();
                return returnTask;
            }
            catch (NonSuccessResponseException ex)
            {
                PeentarLogger.DebugWriteLine($"[{prev.RequestId}]  {prev.Request.Method} {prev.Request.RequestUri} took {prev.ElapsedMs} ms.");
                prev.Dispose();
                throw ex;
            }
            catch (Exception ex)
            {
                prev.Dispose();
                throw ex;
            }
        }

        private static async Task<string> AwaitStringContentInternal(HttpRequestEnvelope envelope, CancellationToken ct)
        {
            HttpResponseMessage res;
            string content;

            try
            {
                res = await envelope.SendAsync(ct);
            }
            // Throw generic authentication exception.
            // WARNING: Presedence matters.
            catch (AggregateException ex)
            {
                throw new NetworkProblemException("Timeout!", envelope, ex);
            }
            catch (TaskCanceledException ex)
            {
                throw ex;
            }
            catch (System.Net.WebException ex)
            {
                throw new NetworkProblemException("WebException", envelope, ex);
            }

            try
            {
                content = await res.Content.ReadAsStringAsync();
            }
            // Throw generic authentication exception.
            // WARNING: Presedence matters.
            catch (AggregateException ex)
            {
                throw new NetworkProblemException("Timeout!", envelope, ex);
            }
            catch (TaskCanceledException ex)
            {
                throw ex;
            }
            catch (System.Net.WebException ex)
            {
                throw new NetworkProblemException("WebException", envelope, ex);
            }

            PeentarLogger.DebugWriteLine($"< [{envelope.RequestId}] {(int)res.StatusCode} {res.ReasonPhrase}");
            PeentarLogger.DebugWriteLine($"< [{envelope.RequestId}] {content}");

            if (res.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new BadRequestException();
            }
            else if (res.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException();
            }
            else if (res.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                throw new ForbiddenException();
            }
            else if (res.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new NotFoundException();
            }
            else if (res.StatusCode == System.Net.HttpStatusCode.MethodNotAllowed)
            {
                throw new ImpossibleStateException($"Route not found for status code {res.StatusCode.ToString()}");
            }

            // Add global network exception here

            /**
             * We need to put non - 200 - ish status code to custom exception
             * since we don't really know what happen to the response.
             * Is it expected? Is it not? It is not the same with
             * NetworkProblemException since NetworkProblemException includes
             * DNS Failure and timeout. The purpose of Non-success Response
             * Exception is to distinguish "no response" and "response exists, but failed".
             *
             * Any 5xx response can be treated as non-success too, because it
             * cannot be fixed from client side
             */
            if (!res.IsSuccessStatusCode)
            {
                var ex = new NonSuccessResponseException(res.StatusCode, envelope)
                {
                    Response = res
                };
                throw ex;
            }
            return content;
        }
    }
}
