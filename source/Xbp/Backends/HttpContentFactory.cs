﻿using Xbp.Util.Json;
using System.Net.Http;
using System.Text;

namespace Xbp.Backends
{
    public class HttpContentFactory
    {
        public static HttpContent FromJsonString(string str)
        {
            return new StringContent(str, Encoding.UTF8, "application/json");
        }

        public static HttpContent FromObjectToJson<T>(T obj)
        {
            var str = PeentarJsonConverter.SerializeObject(obj);
            return FromJsonString(str);
        }

        public static HttpContent FromBytes(byte[] fileBytes)
        {
            return new ByteArrayContent(fileBytes);
        }
    }
}
