﻿using MvvmCross;
using Xbp.Service;
using Xbp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Xbp.Backends
{
    public static class HttpRequest
    {
        private static IUriProvider _uriProvider => Mvx.Resolve<IUriProvider>();
        private static IConnectivityService _connectivityService => Mvx.Resolve<IConnectivityService>();

        /// <summary>
        /// Send http request to backend
        /// </summary>
        /// <param name="htc"></param>
        /// <param name="method"></param>
        /// <param name="endpoint">relative uri</param>
        /// <param name="content">request body, if any</param>
        /// <param name="headers">headers, e.g. auth, if any</param>
        /// <returns></returns>
        public static HttpRequestEnvelope SendAsyncFromUriProvider(this HttpClient htc,
            HttpMethod method,
            string endpoint,
            HttpContent content = null,
            Dictionary<string, string> headers = null)
        {
            if (!_connectivityService.IsConnected())
            {
                throw new NoConnectionAvailableException();
            }

            endpoint = $"{_uriProvider.BaseVersion}{endpoint}";

            var uri = _uriProvider.Provide(endpoint);

            var req = new HttpRequestMessage(method, uri);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    req.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            string readContent = null;

            if (content != null)
            {
                req.Content = content;
                var t = content.ReadAsStringAsync();
                t.Wait();
                readContent = t.Result;
            }

            var envelope = new HttpRequestEnvelope(htc, req);

            PeentarLogger.DebugWriteLine($"[{envelope.RequestId}]> {req.Method} {req.RequestUri}");
            PeentarLogger.DebugWriteLine($"[{envelope.RequestId}]> {req.Headers.ToString()}");
            PeentarLogger.DebugWriteLine($"[{envelope.RequestId}]> {readContent}");

            return envelope;
        }

        /// <summary>
        /// For multiple value query
        /// e.g. ...?test=a,b,c
        /// </summary>
        /// <param name="env"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        public static HttpRequestEnvelope WithQueryString(this HttpRequestEnvelope env, Dictionary<string, List<string>> queryString)
        {
            var queryDictionary = queryString
                .Select(x =>
                {
                    var s = x.Value.Aggregate(new StringBuilder(), (workingSentence, next) =>
                    {
                        if (workingSentence.Length > 0) workingSentence.Append(",");
                        workingSentence.Append(next);
                        return workingSentence;
                    }).ToString();
                    return new KeyValuePair<string, string>(x.Key, s);
                }
                );
            return WithQueryStringInternal(env, queryDictionary);
        }

        public static HttpRequestEnvelope WithQueryString(this HttpRequestEnvelope env, Dictionary<string, string> queryString)
        {
            var queryStringArray = queryString.Select(x => new KeyValuePair<string, string>(x.Key, x.Value));
            return WithQueryStringInternal(env, queryString);
        }

        private static HttpRequestEnvelope WithQueryStringInternal(HttpRequestEnvelope env, IEnumerable<KeyValuePair<string, string>> queryStringArray)
        {
            var realUri = _uriProvider.Provide(env.Request.RequestUri.AbsolutePath, queryStringArray);
            env.Request.RequestUri = new Uri(realUri);
            PeentarLogger.DebugWriteLine($"[{env.RequestId}]> WITHQUERY {env.Request.RequestUri}");
            return env;
        }
    }
}
