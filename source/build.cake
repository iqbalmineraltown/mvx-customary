
#tool "nuget:?package=xunit.runner.console"

var projectPath = "./Xbp.Android";
var projectFile = $"{projectPath}/Xbp.Android.csproj";
var testProjectName = "Xbp.Test";
var testProjectPath = "./Xbp.Test";
var testProjectFile = $"{testProjectPath}/Xbp.Test.csproj";
var solutionFile = "./Xbp.sln";

// this used on ForceClean
Action<Action> IgnoreException = (action) =>
{
  try
  {
    action();
  }
  catch (Exception)
  {
    Console.WriteLine("Exception, but was ignored.");
  }
};

Task("Clean")
  .Does(() =>
{
  MSBuild(projectFile, new MSBuildSettings()
    .WithTarget("Clean")
    .WithProperty("AndroidSdkDirectory", EnvironmentVariable("ANDROID_HOME"))
    .SetConfiguration("Debug"));
});

Task("ForceClean")
  .Does(() =>
{
  var delDirSettings = new DeleteDirectorySettings {
    Recursive = true,
    Force = true
  };

  IgnoreException(() => DeleteDirectory($"{projectPath}/bin", delDirSettings));
  IgnoreException(() => DeleteDirectory($"{projectPath}/obj", delDirSettings));
  IgnoreException(() => DeleteDirectory(".vs", delDirSettings));
});

Task("SetupDev")
  .IsDependentOn("ForceClean")
  .Does(() =>
{
  // Build for the first time to refresh Resources.Designer.cs
  MSBuild(projectFile, new MSBuildSettings().WithRestore());
});

Task("Test")
  .Does(() =>
{
  var htmlReports = $"../Reports/{testProjectName}.dll.html";
  var xmlReports = $"../Reports/{testProjectName}.dll.xml";

  MSBuild(testProjectFile, new MSBuildSettings()
    .WithTarget("Clean"));
  MSBuild(testProjectFile);

  IgnoreException(() => DeleteDirectory("./Reports", true));
  CreateDirectory("./Reports");

  DotNetCoreTool(testProjectFile
    , "xunit", $"--fx-version 2.0.3 --no-build -noshadow -html {htmlReports} -xml {xmlReports}");
});

Task("Default")
  .IsDependentOn("SetupDev")
  .Does(() =>
{
  Information("");
  Information("");
  Information("                ====== SETUP DONE ====== ");
  Information("          Welcome to Peentar Mobile Apps Team");
  Information("                       Have fun!");
  Information("");
  Information("");
});

var target = Argument("target", "Default");
RunTarget(target);