﻿using Xbp.Util;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Xbp.Test.UtilTest
{
    public class ReflectionTest
    {
        [Fact]
        public void TestInvokeGeneric()
        {
            var subjects = new List<Type>
            {
                typeof(TextWriter),
            };

            var testCount = default(int);

            foreach (var subject in subjects)
            {
                var target = Activator.CreateInstance(subject) as IWriter;
                var parameters = new[] { "print text" };
                ReflectionHelper.InvokeGenericMethod(target, nameof(IWriter.Write), typeof(string), parameters);
                testCount++;
            }

            Assert.Equal(testCount, subjects.Count);
        }

        private interface IWriter
        {
            T Write<T>(T obj);
        }

        private class TextWriter : IWriter
        {
            public T Write<T>(T obj)
            {
                if (obj.ToString() != "print text")
                {
                    throw new Exception("wrong object");
                }
                return obj;
            }
        }
    }
}
