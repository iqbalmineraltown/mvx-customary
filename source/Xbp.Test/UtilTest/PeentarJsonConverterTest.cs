using Xbp.Util.Json;
using System;
using Xunit;

namespace Xbp.UtilTest
{
    public class PeentarJsonConverterTest
    {
        #region OmnichannelEnumConverterTest

        public enum HumanType
        {
            VeryCute,
            Adorable,
            SeriouslyVeryCute,
            Dazzling,
            failure,
        }

        public class EnumObject
        {
            public HumanType HumanType { get; set; }
            public HumanType NanaMizuki { get; set; }
            public HumanType Random { get; set; }
        }

        [Fact]
        public void TestOmnichannelEnumSerializationSuccess()
        {
            var o = new EnumObject()
            {
                HumanType = HumanType.VeryCute,
                NanaMizuki = HumanType.Adorable,
                Random = HumanType.SeriouslyVeryCute,
            };

            var input = PeentarJsonConverter.SerializeObject(o);
            var expected = "{\"humanType\":\"VERY_CUTE\",\"nanaMizuki\":\"ADORABLE\",\"random\":\"SERIOUSLY_VERY_CUTE\"}";
            Assert.Equal(expected, input);
        }

        [Fact]
        public void TestOmnichannelEnumSerializationFailUnconventionalFormat()
        {
            var o = new EnumObject()
            {
                HumanType = HumanType.failure
            };

            Assert.Throws<Xbp.Util.ImpossibleStateException>(() => PeentarJsonConverter.SerializeObject(o));
        }

        [Fact]
        public void TestOmnichannelEnumDeserializationSuccess()
        {
            var expected = new EnumObject()
            {
                HumanType = HumanType.VeryCute,
                NanaMizuki = HumanType.Adorable,
                Random = HumanType.SeriouslyVeryCute,
            };

            var json = "{\"humanType\":\"VERY_CUTE\",\"nanaMizuki\":\"ADORABLE\",\"random\":\"SERIOUSLY_VERY_CUTE\"}";
            var input = PeentarJsonConverter.DeserializeObject<EnumObject>(json);
            Assert.Equal(HumanType.VeryCute, expected.HumanType);
            Assert.Equal(HumanType.Adorable, expected.NanaMizuki);
            Assert.Equal(HumanType.SeriouslyVeryCute, expected.Random);
        }

        [Fact]
        public void TestOmnichannelEnumDeserializationFailDoubleUnderscore()
        {
            var json = "{\"humanType\":\"VERY__CUTE\",\"nanaMizuki\":\"ADORABLE\",\"random\":\"SERIOUSLY_VERY_CUTE\"}";
            Assert.Throws<Xbp.Util.ImpossibleStateException>(() => PeentarJsonConverter.DeserializeObject<EnumObject>(json));
        }

        [Fact]
        public void TestOmnichannelEnumDeserializationFailNullForNonNullable()
        {
            var json = "{\"humanType\":null,\"nanaMizuki\":\"ADORABLE\",\"random\":\"SERIOUSLY_VERY_CUTE\"}";
            Assert.Throws<Xbp.Util.ImpossibleStateException>(() => PeentarJsonConverter.DeserializeObject<EnumObject>(json));
        }

        [Fact]
        public void TestOmnichannelEnumDeserializationFailInteger()
        {
            var json = "{\"humanType\":1,\"nanaMizuki\":\"ADORABLE\",\"random\":\"SERIOUSLY_VERY_CUTE\"}";
            Assert.Throws<Xbp.Util.ImpossibleStateException>(() => PeentarJsonConverter.DeserializeObject<EnumObject>(json));
        }

        #endregion OmnichannelEnumConverterTest

        #region DateTimeOffsetTest

        public class DateObject
        {
            public DateTimeOffset TimeStamp { get; set; }
        }

        [Fact]
        public void TestTimeSerialization()
        {
            var o = new DateObject() { TimeStamp = new DateTimeOffset(2016, 6, 1, 10, 0, 0, new TimeSpan(7, 0, 0)) };
            var res = PeentarJsonConverter.SerializeObject(o);
            Assert.Equal("{\"timeStamp\":\"2016-06-01T10:00:00+07:00\"}", res);
        }

        [Fact]
        public void TestTimeDeserialization()
        {
            var expected = new DateObject() { TimeStamp = new DateTimeOffset(2016, 6, 1, 10, 0, 0, new TimeSpan(7, 0, 0)) };

            var json = "{\"timeStamp\":\"2016-06-01T10:00:00+07:00\"}";
            var res = PeentarJsonConverter.DeserializeObject<DateObject>(json);
            Assert.Equal(res.TimeStamp, expected.TimeStamp);
        }

        #endregion DateTimeOffsetTest
    }
}
