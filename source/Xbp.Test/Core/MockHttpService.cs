﻿using Moq;
using MvvmCross;
using Xbp.Backends;
using Xbp.Service;
using Xbp.Test.Fixtures;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Xbp.Test.Core
{
    public class MockHttpService
    {
        public MockHttpService()
        {
            MockObject = new Mock<IHttpClientFactory>();
            Handler = new MockHttpHandler();
            MockObject.Setup(x => x.CreateHttpClient(It.IsAny<int>()))
                .Returns(new HttpClient(Handler));

            Mvx.ConstructAndRegisterSingleton<IUriProvider, UriProvider>();
            var mockUri = Mvx.Resolve<IUriProvider>();
            mockUri.BaseUrl = "http://mock.endpoint.url";
            mockUri.BaseResourceUrl = "http://mock.resource.url";
            mockUri.BaseVersion = "/mock/version";

            var connectivityServiceMock = new Mock<IConnectivityService>(MockBehavior.Strict);
            connectivityServiceMock.Setup(x => x.IsConnected()).Returns(true);
            Mvx.RegisterSingleton(connectivityServiceMock.Object);
        }

        public void Allow(JsonFixtureBase fixture)
        {
            Handler.Allow(fixture);
        }

        private HttpClient CreateMockHttpClient(JsonFixtureBase fixture)
        {
            var resp = new HttpResponseMessage(fixture.ResponseCode);
            var content = new StringContent(fixture.JsonResponse, Encoding.UTF8, "application/json");
            resp.Content = content;

            var mockHandler = new MockHttpHandler();
            mockHandler.Allow(fixture);

            var result = new HttpClient(mockHandler);

            return result;
        }

        public Mock<IHttpClientFactory> MockObject { get; set; }
        private MockHttpHandler Handler { get; set; }
    }

    internal class MockHttpHandler : HttpMessageHandler
    {
        private List<JsonFixtureBase> AllowedMessages { get; set; }

        public MockHttpHandler() : base()
        {
            AllowedMessages = new List<JsonFixtureBase>();
        }

        public void Allow(JsonFixtureBase message)
        {
            AllowedMessages.Add(message);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var fixture = AllowedMessages.First(x => IsSubString(request.RequestUri.ToString(), x.UriContains));
            var resp = new HttpResponseMessage(fixture.ResponseCode);
            var content = new StringContent(fixture.JsonResponse, Encoding.UTF8, "application/json");
            resp.Content = content;

            return Task.FromResult(resp);
        }

        private bool IsSubString(string string1, string string2)
        {
            var longString = default(string);
            var shortString = default(string);
            if (string1 == string2)
            {
                return true;
            }
            else if (string1.Length < string2.Length)
            {
                longString = string2;
                shortString = string1;
            }
            else if (string1.Length > string2.Length)
            {
                longString = string1;
                shortString = string2;
            }
            else // string1 != string2
            {
                return false;
            }

            return longString.Contains(shortString);
        }
    }
}
