﻿using MvvmCross.Base;
using MvvmCross.Tests;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;

namespace Xbp.Test.Core
{
    public class BaseMvxTest : MvxIoCSupportingTest
    {
        protected MvxMainThreadDispatcher CreateMockNavigation()
        {
            return new MockDispatcher();
        }
    }

    internal class MockDispatcher : MvxMainThreadDispatcher
    {
        public List<MvxViewModelRequest> Requests => new List<MvxViewModelRequest>();
        public List<MvxPresentationHint> Hints => new List<MvxPresentationHint>();

        public override bool IsOnMainThread => true;

        public override bool RequestMainThreadAction(Action action, bool maskExceptions = true)
        {
            action();
            return true;
        }
    }
}
