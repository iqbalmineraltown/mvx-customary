﻿using System.Net;

namespace Xbp.Test.Fixtures
{
    public abstract class JsonFixtureBase
    {
        public virtual HttpStatusCode ResponseCode => HttpStatusCode.OK;
        public virtual string JsonResponse => string.Empty;
        public abstract string UriContains { get; }
    }
}
