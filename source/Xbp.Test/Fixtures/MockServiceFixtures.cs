﻿using System.Net;

namespace Xbp.Test.Fixtures
{
    public class GetStatusBase : JsonFixtureBase
    {
        public override string UriContains => "/mock/get-status";
    }

    public class GetStatusSuccess : GetStatusBase
    {
        public override string JsonResponse => "true";
    }

    /// Fixtures below should throw related http code exception

    public class GetStatusBadRequest : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.BadRequest;
    }

    public class GetStatusUnauthorized : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.Unauthorized;
    }

    public class GetStatusForbidden : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.Forbidden;
    }

    public class GetStatusNotFound : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.NotFound;
    }

    /// Fixtures below should throw NonSuccessException

    public class GetStatusInternalServerError : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.InternalServerError;
    }

    public class GetStatusBadGateway : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.BadGateway;
    }

    public class GetStatusGatewayTimeout : GetStatusBase
    {
        public override HttpStatusCode ResponseCode => HttpStatusCode.GatewayTimeout;
    }
}
