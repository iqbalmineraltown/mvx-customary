﻿using Xbp.Backends;
using Xbp.Util.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Xbp.Test.ServiceTest
{
    /// <summary>
    /// Sample service to test service
    /// </summary>
    public interface IMockService
    {
        Task<bool> GetIsEndpointOk();
    }

    public class MockService : IMockService
    {
        private IHttpClientFactory _htcFactory;

        public MockService(IHttpClientFactory htcFactory)
        {
            _htcFactory = htcFactory;
        }

        public async Task<bool> GetIsEndpointOk()
        {
            using (var htc = _htcFactory.CreateHttpClient())
            {
                var res = await htc
                    .SendAsyncFromUriProvider(HttpMethod.Get, "/mock/get-status")
                    .AwaitStringContent();

                return PeentarJsonConverter.DeserializeObject<bool>(res); ;
            }
        }
    }
}
