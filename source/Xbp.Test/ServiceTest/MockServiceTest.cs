﻿using Moq;
using MvvmCross;
using Xbp.Service;
using Xbp.Util;
using Xbp.Test.Core;
using Xbp.Test.Fixtures;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Xbp.Test.ServiceTest
{
    public class MockServiceTest : BaseMvxTest
    {
        public MockServiceTest()
        {
            ClearAll();

            MockHttpService = new MockHttpService();
            Mvx.RegisterSingleton(MockHttpService.MockObject.Object);

            Mvx.ConstructAndRegisterSingleton<IMockService, MockService>();
        }

        protected MockHttpService MockHttpService { get; set; }
        protected IMockService MockService => Mvx.Resolve<IMockService>();

        [Fact]
        public async Task NoConnectionAvailableGetStatusTest()
        {
            var connectivityServiceMock = new Mock<IConnectivityService>(MockBehavior.Strict);
            connectivityServiceMock.Setup(x => x.IsConnected()).Returns(false);
            Mvx.RegisterSingleton(connectivityServiceMock.Object);

            await Assert.ThrowsAsync<NoConnectionAvailableException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task SuccessGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusSuccess());

            var res = await MockService.GetIsEndpointOk();

            Assert.True(res);
        }

        [Fact]
        public async Task FailedBadRequestGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusBadRequest());

            await Assert.ThrowsAsync<BadRequestException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedUnauthorizedGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusUnauthorized());

            await Assert.ThrowsAsync<UnauthorizedException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedForbiddenGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusForbidden());

            await Assert.ThrowsAsync<ForbiddenException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedNotFoundGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusNotFound());

            await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedInternalServerErrorGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusInternalServerError());

            await Assert.ThrowsAsync<NonSuccessResponseException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedBadGatewayGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusBadGateway());

            await Assert.ThrowsAsync<NonSuccessResponseException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }

        [Fact]
        public async Task FailedGatewayTimeoutGetStatusTest()
        {
            MockHttpService.Allow(new GetStatusGatewayTimeout());

            await Assert.ThrowsAsync<NonSuccessResponseException>(async () =>
            {
                await MockService.GetIsEndpointOk();
            });
        }
    }
}
