# Creating new Xamarin + Mvvmcross 6 project

## Creating PCL project

1. Open Visual Studio
1. `File` -> `New` -> `Project...` or `Ctrl+Shift+N`
1. Choose `.Net Standard` in left tab
1. Name the project and adjust location, then click OK
1. After opened, delete `Class1.cs` File
1. Right click on PCL project -> `Manage NuGet packages..`
1. Find `MvvmCross` and install it
1. Right click on PCL project -> `Add` -> `Class...` or `Alt+Shift+C`
1. Name it `App.cs` and copy code below (adjust namespace to your project)

    ``` C#
    using MvvmCross.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Text;

    namespace NewProject.MK1
    {
        public class App : MvxApplication
        {
        }
    }
    ```

1. Add your first `ViewModel`
1. Proceed to creating android project

## Creating Android Project

1. Right click on `Solution {SolutionName}` in Solution Explorer window
1. `Add` -> `New Project...`
1. Choose `Android` in left tab
1. Choose `Blank App (Android)`
1. Name the project and adjust location, then click OK
1. Delete `MainActivity.cs`, `Main.axml` files
1. Right click on `References` -> `Add References..`
1. In left tab choose `Projects` -> `Solution` then choose your PCL project
1. Right click on Android project -> `Manage NuGet packages..`
1. Find `MvvmCross.Droid.Support.V7.AppCompat` and install it
1. Right click on Android project -> `Add` -> `Class...` or `Alt+Shift+C`
1. Name it `Setup.cs` and copy code below (adjust namespace to your project)

    ``` C#
    using MvvmCross.Droid.Support.V7.AppCompat;

    namespace NewProject.MK1.Android
    {
        public class Setup : MvxAppCompatSetup<App>
        {
        }
    }
    ```
1. Add another class, name it `MainApplication.cs` and copy code below (adjust namespace to your project)

    ``` C#
    using Android.App;
    using MvvmCross.Droid.Support.V7.AppCompat;

    namespace NewProject.MK1.Android
    {
        [Application]
        public class MainApplication : MvxAppCompatApplication<Setup, App>
        {
        }
    }
    ```

1. Add `LinkerPleaseInclude.cs` file
    - You can use sample from MvvmCross
    [Link](https://github.com/MvvmCross/MvvmCross/blob/develop/ContentFiles/Android/LinkerPleaseInclude.cs)

1. Add `Styles.xml` for your application theme
1. Add application icon to drawable resource folder, name it `Icon.png`
1. Add splash screen if needed
1. Add `Mono.Android.Export.dll` in reference, in `Assemblies` -> `Framework` tab
1. Create your first `Activity`

## Backend Module

1. On `App.cs`, copy code below (adjust namespace to your project)

    ``` C#
    namespace NewProject.MK1.Android
    {
        public class App : MvxApplication
        {
            public override void Initialize()
            {
                base.Initialize();

                CreatableTypes()
                    .EndingWith("Service") // service filename endings
                    .AsInterfaces()
                    .RegisterAsLazySingleton();

                Mvx.LazyConstructAndRegisterSingleton<IHttpClientFactory>(() => new HttpClientFactory());
                Mvx.LazyConstructAndRegisterSingleton<IUriProvider>(() => new UriProvider());

                var uriProvider = Mvx.Resolve<IUriProvider>();
                uriProvider.BaseUrl = "http://base.endpoint.url"; // base endpoint url
                uriProvider.BaseVersion = "/v1"; // versioning, if exist

                RegisterAppStart<MainViewModel>(); //change to your first viewmodel
            }
        }
    }
    ```

1. Create `service`-type interface and class. As example, we can use `ISampleService` from `sample`. We are using filename convention with `service` endings to register service singleton as stated in previous step.

    ``` C#
    public interface ISampleService
    {
        Task<string> GetStringAsync();
    }

    public class SampleService : ISampleService
    {
        private IHttpClientFactory _httpClientFactory;

        public SampleService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<string> GetStringAsync()
        {
            using (var htc = _httpClientFactory.CreateHttpClient())
            {
                var res = await htc
                    .SendAsyncFromUriProvider(HttpMethod.Get, "/endpoint") // replace with relative path endpoint
                    .AwaitStringContent();

                return res;
            }
        }
    }
    ```

1. Now we can use service in ViewModel via dependency injection