﻿using Xbp.Backends;
using Xbp.Util.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PeentarXbpSample.Services
{
    public interface ISampleService
    {
        Task<string> GetStringAsync(CancellationToken ct);
    }

    public class SampleService : ISampleService
    {
        private IHttpClientFactory _httpClientFactory;

        public SampleService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<string> GetStringAsync(CancellationToken ct)
        {
            using (var htc = _httpClientFactory.CreateHttpClient())
            {
                var header = new Dictionary<string, string>
                {
                    { "PeentarHeader", "headerapaaja" },
                };

                var query = new Dictionary<string, string>
                {
                    { "mocky-delay", "1000ms" }
                };

                var res = await htc
                    .SendAsyncFromUriProvider(HttpMethod.Get, "/5ae01319320000730051096e", headers: header)
                    .WithQueryString(query)
                    .AwaitStringContent(ct);

                return PeentarJsonConverter.DeserializeObject<SampleDto>(res).Message;
            }
        }

        private class SampleDto
        {
            public string Message { get; set; }
        }
    }
}
