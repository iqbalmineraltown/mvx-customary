﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using Xbp.Core;
using Xbp.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    /// <summary>
    /// For tab layout with activity parent
    /// </summary>
    public class TabPageContainerViewModel : BaseViewModel
    {
        private List<string> _links = new List<string>
        {
            "https://imgur.com/DeKVLBz.png",
            "https://imgur.com/S3WuJIG.png",
            "https://imgur.com/53tCCdk.png",
            "https://imgur.com/CMjH7Ks.png",
            "https://imgur.com/xED3vN8.png",
            "https://imgur.com/VOFy7e5.png",
            "https://imgur.com/7wIKLNR.png",
            "https://imgur.com/jESfzsQ.png",
            "https://imgur.com/O70V63r.png",
        };

        public override void PostViewCreated()
        {
            base.PostViewCreated();
            MvxNotifyTask.Create(async () =>
            {
                await DoShowInitialViewModel();
            }, HandleException);
        }

        private void HandleException(Exception obj)
        {
            PeentarLogger.DebugWriteLine($"{obj.GetType()}: {obj.Message}");
        }

        public IMvxCommand ShowInitialViewModelCommand => new MvxAsyncCommand(DoShowInitialViewModel);

        private async Task DoShowInitialViewModel()
        {
            var tasks = new List<Task>();
            var count = 0;
            foreach (var s in _links)
            {
                tasks.Add(Navigate<TabPageViewModel, TabParam>(new TabParam { TabTitle = $"Tab {++count}", ContentLink = s }));
            }

            await Task.WhenAll(tasks);
        }
    }

    public class TabParam
    {
        public string TabTitle { get; set; }
        public string ContentLink { get; set; }
    }
}
