﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class FirstContentViewModel : BaseViewModel
    {
        private string _imageSource = "https://picsum.photos/200/300/?random";
        public string ImageSource

        {
            get { return _imageSource; }
            set { _imageSource = value; RaisePropertyChanged(() => ImageSource); }
        }
    }
}
