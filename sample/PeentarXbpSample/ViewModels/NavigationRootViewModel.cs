﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using Xbp.Core;
using Xbp.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class NavigationRootViewModel : BaseViewModel
    {
        public override void PostViewCreated()
        {
            MvxNotifyTask.Create(async () =>
            {
                await DoShowMenu();
                await DoShowFirstContent();
            }, OnException);
        }

        private void OnException(Exception obj)
        {
            PeentarLogger.DebugWriteLine($"{obj.GetType()}: {obj.Message}");
        }

        public IMvxCommand ShowMenuCommand => new MvxAsyncCommand(DoShowMenu);

        private async Task DoShowMenu()
        {
            await Navigate<NavigationViewModel>();
        }

        public IMvxCommand ShowFirstContentCommand => new MvxAsyncCommand(DoShowFirstContent);

        private async Task DoShowFirstContent()
        {
            await Navigate<FirstContentViewModel>();
        }
    }
}
