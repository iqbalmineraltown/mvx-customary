﻿using MvvmCross.Commands;
using Xbp.Core;
using Xbp.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class InAppBrowserViewModel : BaseViewModel<bool>
    {
        public override void Prepare(bool param)
        {
            ShouldAddToBackstack = param;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            TextUrl = "http://Xbp.id/";
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; RaisePropertyChanged(() => IsLoading); }
        }

        private string _urlPath;
        public string UrlPath
        {
            get { return _urlPath; }
            set { _urlPath = value; RaisePropertyChanged(() => UrlPath); }
        }

        private string _textUrl;
        public string TextUrl
        {
            get { return _textUrl; }
            set { _textUrl = value; RaisePropertyChanged(() => TextUrl); }
        }

        private bool _shouldAddToBackstack;
        public bool ShouldAddToBackstack
        {
            get { return _shouldAddToBackstack; }
            set { _shouldAddToBackstack = value; RaisePropertyChanged(() => ShouldAddToBackstack); }
        }

        public IMvxCommand OpenUrlCommand => new MvxCommand(DoOpenUrl);

        private void DoOpenUrl()
        {
            UrlPath = TextUrl;
            IsLoading = false;
        }

        public IMvxCommand FinishLoadCommand => new MvxCommand<string>(DoFinishLoad);

        private void DoFinishLoad(string param)
        {
            DialogService.ShowToast($"Loaded URL: {param}");
            IsLoading = false;
        }

        public IMvxCommand ReloadPageCommand => new MvxCommand(DoReloadPage);
        private void DoReloadPage()
        {
            UrlPath = $"http://www.convert-unix-time.com/api?timestamp=now";
        }
    }
}
