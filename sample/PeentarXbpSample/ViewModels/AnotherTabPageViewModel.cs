﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    /// <summary>
    /// For tab layout with fragment parent
    /// </summary>
    public class AnotherTabPageViewModel : BaseViewModel
    {
        public void Init(string param)
        {
            ContentLink = param;
        }

        public bool ShouldShow => true;

        private string _contentLink;
        public string ContentLink
        {
            get { return _contentLink; }
            set { _contentLink = value; RaisePropertyChanged(() => ContentLink); }
        }

        public IMvxCommand OpenBrowserCommand => new MvxAsyncCommand(DoOpenBrowser);

        private async Task DoOpenBrowser()
        {
            await Navigate<InAppBrowserViewModel, bool>(true);
        }
    }
}
