﻿using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace PeentarXbpSample.ViewModels
{
    public class TabPageViewModel : BaseViewModel<TabParam>
    {
        public override void Prepare(TabParam param)
        {
            TabTitle = param.TabTitle;
            ContentLink = param.ContentLink;
        }

        private string _tabTitle;
        public string TabTitle
        {
            get { return _tabTitle; }
            set { _tabTitle = value; RaisePropertyChanged(() => TabTitle); }
        }

        private string _contentLink;
        public string ContentLink
        {
            get { return _contentLink; }
            set { _contentLink = value; RaisePropertyChanged(() => ContentLink); }
        }
    }
}
