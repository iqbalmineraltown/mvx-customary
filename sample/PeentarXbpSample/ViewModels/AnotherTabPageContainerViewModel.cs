﻿using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace PeentarXbpSample.ViewModels
{
    /// <summary>
    /// For tab layout with fragment parent
    /// </summary>
    public class AnotherTabPageContainerViewModel : BaseViewModel
    {
        public List<string> Links => new List<string>
        {
            "https://imgur.com/DeKVLBz.png",
            "https://imgur.com/S3WuJIG.png",
            "https://imgur.com/53tCCdk.png",
            "https://imgur.com/CMjH7Ks.png",
            "https://imgur.com/xED3vN8.png",
            "https://imgur.com/VOFy7e5.png",
            "https://imgur.com/7wIKLNR.png",
            "https://imgur.com/jESfzsQ.png",
            "https://imgur.com/O70V63r.png",
        };
    }
}
