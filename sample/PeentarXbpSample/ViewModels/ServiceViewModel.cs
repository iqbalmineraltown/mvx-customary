﻿using MvvmCross.Commands;
using MvvmCross.UI;
using Xbp.Core;
using Xbp.Service;
using Xbp.Util;
using PeentarXbpSample.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class ServiceViewModel : BaseViewModel
    {
        private ISampleService _sampleService;

        public ServiceViewModel(ISampleService sampleService)
        {
            _sampleService = sampleService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            Text = "Sedang Memuat...";
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }

        public async Task LoadStringAsync()
        {
            try
            {
                await Task.Delay(2000, CancelToken);
                Text = await _sampleService.GetStringAsync(CancelToken);
                await Task.Delay(2000, CancelToken);
                DialogService.ShowToast(Text);
            }
            catch (NoConnectionAvailableException)
            {
                DialogService.ShowSnackBar("Tidak terhubung dengan jaringan", SnackbarLengthEnum.Indefinite, MvxColors.Green,
                    "Coba Lagi", () => RunAsync(LoadStringAsync, OnExceptionThrown), MvxColors.White,
                    MvxColors.Red);
            }
            catch (NetworkProblemException)
            {
                DialogService.ShowToast("Network Problem");
            }
            catch (TaskCanceledException)
            {
                DialogService.ShowToast("Task Definitely Cancelled");
            }
        }

        private void OnExceptionThrown(Exception ex)
        {
            DialogService.ShowToast("RunAsync Exception Thrown");
        }

        public IMvxCommand StartLoadCommand => new MvxCommand(DoStartLoad);

        private void DoStartLoad()
        {
            RunAsync(LoadStringAsync, OnExceptionThrown);
        }

        public IMvxCommand CancelLoadCommand => new MvxCommand(DoCancelLoad);

        private void DoCancelLoad()
        {
            RequestCancellation();
        }
    }
}
