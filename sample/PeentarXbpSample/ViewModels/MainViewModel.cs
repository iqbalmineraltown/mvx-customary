﻿using MvvmCross;
using MvvmCross.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xbp.Core;
using Xbp.Service;

namespace PeentarXbpSample.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private ISimpleTypedPermanentStorageService _storageService;

        public MainViewModel(ISimpleTypedPermanentStorageService storageService)
        {
            _storageService = storageService;
        }

        public override async Task Initialize()
        {
            //TODO: Add starting logic here

            await base.Initialize();

            var saved = _storageService.Retrieve<string>("saved_choice");
            if (saved != null)
            {
                Text = saved;
            }
        }

        private string _text = "Hello Happy World";
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }

        public IMvxCommand ExitCommand => new MvxAsyncCommand(DoExit);

        private async Task DoExit()
        {
            var conf = await DialogService.ShowConfirmationDialog("", "Quit application?");
            if (conf == ConfirmationResultEnum.Affirmative)
            {
                await CloseSelf();
            }
        }

        public IMvxCommand ResetTextCommand => new MvxCommand(ResetText);

        private void ResetText()
        {
            Text = "Hello Happy World";
        }

        public IMvxCommand OpenDialogCommand => new MvxAsyncCommand(DoOpenDialog);

        private async Task DoOpenDialog()
        {
            await Navigate<DialogViewModel, string>(Text);
        }

        public IMvxCommand EditTextCommand => new MvxAsyncCommand(DoEditText);

        private async Task DoEditText()
        {
            Text = await Navigate<EditViewModel, string>() ?? Text;

            _storageService.Store("saved_choice", Text);

            DialogService.ShowToast("Saved");
        }

        public IMvxCommand RequestLocationPermissionCommand => new MvxAsyncCommand(DoRequestLocationPermission);

        private async Task DoRequestLocationPermission()
        {
            var permisssionService = Mvx.Resolve<IPermissionService>();
            var locationPermissionStatus = await permisssionService.CheckPermissionStatus(PermissionName.Location);
            if (locationPermissionStatus == PermissionStatus.Denied)
            {
                var permissionToRequest = new[] { PermissionName.Location };
                var results = await permisssionService.RequestPermissions(permissionToRequest);
                locationPermissionStatus = results[PermissionName.Location];
            }
            System.Diagnostics.Debug.WriteLine($"Location permission status : {locationPermissionStatus}");
            DialogService.ShowSnackBar($"Location permission status : {locationPermissionStatus}");
        }

        public IMvxCommand ViewImageCommand => new MvxAsyncCommand(DoViewImage);

        private async Task DoViewImage()
        {
            await Navigate<ImageDialogViewModel>();
        }

        public IMvxCommand ServiceCommand => new MvxAsyncCommand(DoService);

        private async Task DoService()
        {
            await Navigate<ServiceViewModel>();
        }

        public IMvxCommand InAppBrowserCommand => new MvxAsyncCommand(DoInAppBrowser);

        private async Task DoInAppBrowser()
        {
            await Navigate<InAppBrowserViewModel>();
        }

        public IMvxCommand ViewTabCommand => new MvxAsyncCommand(DoViewTab);

        private async Task DoViewTab()
        {
            await Navigate<TabPageContainerViewModel>();
        }

        public IMvxCommand ViewAnotherTabCommand => new MvxAsyncCommand(DoViewAnotherTab);

        private async Task DoViewAnotherTab()
        {
            await Navigate<AnotherTabPageContainerViewModel>();
        }

        public IMvxCommand ShowNavDrawerCommand => new MvxAsyncCommand(DoShowNavDrawer);

        private async Task DoShowNavDrawer()
        {
            await Navigate<NavigationRootViewModel>();
        }
    }
}
