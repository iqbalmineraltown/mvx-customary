﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class SecondContentViewModel : BaseViewModel
    {
        public override async Task Initialize()
        {
            await base.Initialize();
            Xbp.Util.PeentarLogger.DebugWriteLine("SecondViewModel");
        }

        public IMvxCommand OpenBrowserCommand => new MvxAsyncCommand(DoOpenBrowser);

        private async Task DoOpenBrowser()
        {
            await Navigate<InAppBrowserViewModel>();
        }

        public IMvxCommand OpenBottomSheetCommand => new MvxAsyncCommand(DoOpenBottomSheet);

        private async Task DoOpenBottomSheet()
        {
            await ChangePresentation(new PresentationHints.TestBottomSheetPresentationHint(true));
        }

        public IMvxCommand CloseBottomSheetCommand => new MvxAsyncCommand(DoCloseBottomSheet);

        private async Task DoCloseBottomSheet()
        {
            await ChangePresentation(new PresentationHints.TestBottomSheetPresentationHint(false));
        }

        public IMvxCommand OpenKeyboardCommand => new MvxAsyncCommand(DoOpenKeyboard);

        private async Task DoOpenKeyboard()
        {
            await ChangePresentation(new PresentationHints.OpenKeyboardPresentationHint());
        }
    }
}
