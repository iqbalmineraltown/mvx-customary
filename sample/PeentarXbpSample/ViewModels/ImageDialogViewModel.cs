﻿using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class ImageDialogViewModel : BaseViewModel
    {
        public override async Task Initialize()
        {
            await base.Initialize();

            ImageUrl = "https://http.cat/301.jpg";
        }

        private string _imageUrl;
        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; RaisePropertyChanged(() => ImageUrl); }
        }
    }
}
