﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class DialogViewModel : BaseViewModel<string>
    {
        public override void Prepare(string param)
        {
            Text = param;
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; RaisePropertyChanged(() => Text); }
        }

        public IMvxCommand CloseCommand => new MvxAsyncCommand(DoClose);

        private async Task DoClose()
        {
            await CloseSelf();
        }
    }
}
