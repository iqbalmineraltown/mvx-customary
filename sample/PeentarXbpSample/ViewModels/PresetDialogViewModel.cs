﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class PresetDialogViewModel : BaseViewModel<string, string>
    {
        public override void Prepare(string parameter)
        {
            PreviousChoice = parameter;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            LoadChoice();
        }

        private List<ChoiceEntry> _choiceList;
        public List<ChoiceEntry> ChoiceList
        {
            get { return _choiceList; }
            set { _choiceList = value; RaisePropertyChanged(() => ChoiceList); }
        }

        private ChoiceEntry _selectedChoice;
        public ChoiceEntry SelectedChoice
        {
            get { return _selectedChoice; }
            set { _selectedChoice = value; RaisePropertyChanged(() => SelectedChoice); }
        }

        private string _previousChoice;
        public string PreviousChoice
        {
            get { return _previousChoice; }
            set { _previousChoice = value; RaisePropertyChanged(() => PreviousChoice); }
        }

        private void LoadChoice()
        {
            RunAsync(LoadChoiceAsync);
        }

        private async Task LoadChoiceAsync()
        {
            await Task.Delay(200);

            var dialog = await DialogService.ShowLoadingDialog("Memuat");
            await Task.Delay(2000);

            var set = new HashSet<string>
            {
                "Poppin' Party",
                "Afterglow",
                "Roselia",
                "Pastel Palettes",
                "Hello Happy World",
            };

            if (PreviousChoice != null)
            {
                set.Add(PreviousChoice);
            }

            ChoiceList = set.Select(x => new ChoiceEntry { Value = x }).ToList();
            SelectedChoice = ChoiceList[PreviousChoice != null ? ChoiceList.FindIndex(x => x.Value == PreviousChoice) : 0];

            DialogService.CloseLoadingDialog(dialog);
        }

        public IMvxCommand CancelCommand => new MvxAsyncCommand(DoCancel);

        private async Task DoCancel()
        {
            await CloseSelf(PreviousChoice);
        }

        public IMvxCommand SaveCommand => new MvxAsyncCommand(DoSave);

        private async Task DoSave()
        {
            await CloseSelf(SelectedChoice.Value);
        }
    }

    public class ChoiceEntry
    {
        public string Value { get; set; }
    }
}
