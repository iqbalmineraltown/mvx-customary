﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class ThirdContentViewModel : BaseViewModel
    {
        public IMvxCommand OpenNextCommand => new MvxAsyncCommand(DoOpenNext);

        private async Task DoOpenNext()
        {
            await Navigate<FourthContentViewModel>();
        }
    }
}
