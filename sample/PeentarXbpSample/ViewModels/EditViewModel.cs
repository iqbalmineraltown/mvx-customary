﻿using MvvmCross.Commands;
using Xbp.Core;
using Xbp.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class EditViewModel : BaseViewModelResult<string>
    {
        public override async Task Initialize()
        {
            await base.Initialize();

            Text = "Afterglow";
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; RaisePropertyChanged(() => Text); }
        }

        public IMvxCommand ChoosePresetCommand => new MvxAsyncCommand(DoChoosePreset);

        private async Task DoChoosePreset()
        {
            var prev = Text;
            Text = await Navigate<PresetDialogViewModel, string, string>(Text) ?? Text;

            DialogService.ShowSnackBar($"Selected {Text}", actionText: "Undo", action: () => Text = prev);
        }

        public IMvxCommand SaveCommand => new MvxAsyncCommand(DoSave);

        private async Task DoSave()
        {
            await CloseSelf(Text);
        }

        public IMvxCommand PreviewCommand => new MvxAsyncCommand(DoPreview);

        private async Task DoPreview()
        {
            DialogService.ShowAlertDialog("Preview", Text);
        }
    }
}
