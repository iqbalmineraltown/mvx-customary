﻿using MvvmCross.Commands;
using Xbp.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PeentarXbpSample.ViewModels
{
    public class NavigationViewModel : BaseViewModel
    {
        public IMvxCommand ShowFirstContentCommand => new MvxAsyncCommand(DoShowFirstContent);

        private async Task DoShowFirstContent()
        {
            await Navigate<FirstContentViewModel>();
        }

        public IMvxCommand ShowSecondContentCommand => new MvxAsyncCommand(DoShowSecondContent);

        private async Task DoShowSecondContent()
        {
            await Navigate<SecondContentViewModel>();
        }

        public IMvxCommand ShowThirdContentCommand => new MvxAsyncCommand(DoShowThirdContent);

        private async Task DoShowThirdContent()
        {
            await Navigate<ThirdContentViewModel>();
        }
    }
}
