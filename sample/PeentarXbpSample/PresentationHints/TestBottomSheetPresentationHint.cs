﻿using Xbp.PresentationHint;
using System;
using System.Collections.Generic;
using System.Text;

namespace PeentarXbpSample.PresentationHints
{
    public class TestBottomSheetPresentationHint : BaseBottomSheetPresentationHint
    {
        public TestBottomSheetPresentationHint(bool isExpanded) : base(isExpanded)
        {
            IsExpanded = isExpanded;
        }

        public override bool IsExpanded { get; set; }
    }
}
