﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using PeentarXbpSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xbp.Backends;
using Xbp.Core;

namespace PeentarXbpSample
{
    public class App : BaseMvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            var uriProvider = Mvx.Resolve<IUriProvider>();
            uriProvider.BaseUrl = "http://www.mocky.io";
            uriProvider.BaseVersion = "/v2";

            RegisterAppStart<MainViewModel>();
        }
    }
}
