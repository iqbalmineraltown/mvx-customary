﻿using System.Collections.Generic;
using MvvmCross;
using Xbp.Android.Core;
using MvvmCross.Platforms.Android;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Platforms.Android.Core;
using Xbp.PresentationHint;
using Xbp.Android.PresentationHandlers;
using PeentarXbpSample.Android.PresentationHandlers;
using PeentarXbpSample.PresentationHints;

namespace PeentarXbpSample.Android
{
    public class Setup : BaseMvxAndroidSetup<App>
    {
        protected override Dictionary<BasePresentationHint, BasePresentationHandler> CustomPresentations
            => new Dictionary<BasePresentationHint, BasePresentationHandler>()
            {
                { new TestBottomSheetPresentationHint(false), new TestBottomSheetPresentationHandler() },
                { new OpenKeyboardPresentationHint(), new OpenKeyboardPresentationHandler() }
            };

        /// <summary>
        /// This is the old implementation of IMvxAndroidCurrentTopActivity.
        /// Using the new one https://github.com/MvvmCross/MvvmCross/issues/783
        /// creates problem with AndroidSimpleTypedPermanentStorageService.
        /// Example case: Activity A opens Activity B. If Activity B is closed and in Activity A's ViewModel called Store(), NPE will be thrown.
        /// That's because CurrentContext value there is null
        /// </summary>
        /// <returns></returns>
        protected override IMvxAndroidCurrentTopActivity CreateAndroidCurrentTopActivity()
        {
            return new MvxLifecycleMonitorCurrentTopActivity(Mvx.GetSingleton<IMvxAndroidActivityLifetimeListener>());
        }
    }
}
