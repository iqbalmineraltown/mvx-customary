﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xbp.Android.PresentationHandlers;
using Xbp.PresentationHint;
using PeentarXbpSample.PresentationHints;

namespace PeentarXbpSample.Android.PresentationHandlers
{
    public class OpenKeyboardPresentationHandler : BasePresentationHandler
    {
        private MvvmCross.Droid.Support.V7.AppCompat.MvxAppCompatActivity Activity
            => Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity as MvvmCross.Droid.Support.V7.AppCompat.MvxAppCompatActivity;

        public override Func<BasePresentationHint, bool> Handler => (presentationHint) =>
        {
            var hint = presentationHint as OpenKeyboardPresentationHint;
            var imm = (InputMethodManager)Activity.GetSystemService(Context.InputMethodService);
            imm.ToggleSoftInput(ShowFlags.Forced, 0);
            return true;
        };
    }
}
