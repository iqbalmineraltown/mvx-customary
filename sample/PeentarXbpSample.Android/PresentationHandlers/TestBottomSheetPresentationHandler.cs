﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using Xbp.Android.PresentationHandlers;
using Xbp.Android.Util;
using Xbp.PresentationHint;
using PeentarXbpSample.Android.Views;
using PeentarXbpSample.PresentationHints;

namespace PeentarXbpSample.Android.PresentationHandlers
{
    public class TestBottomSheetPresentationHandler : BasePresentationHandler
    {
        private MvvmCross.Droid.Support.V7.AppCompat.MvxAppCompatActivity Activity
            => Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity as MvvmCross.Droid.Support.V7.AppCompat.MvxAppCompatActivity;

        public override Func<BasePresentationHint, bool> Handler => (presentationHint) =>
        {
            var testBottomSheet = AndroidUtils.GetFragmentOf<ITestBottomSheet>(Activity);
            if (testBottomSheet != null)
            {
                testBottomSheet.IsExpanded = (presentationHint as TestBottomSheetPresentationHint)?.IsExpanded ?? false;
            }
            return true;
        };
    }
}
