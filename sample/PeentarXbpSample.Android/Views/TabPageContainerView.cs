﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [Activity]
    public class TabPageContainerView : BaseMvxActivity<TabPageContainerViewModel>
    {
        protected override int LayoutId => Resource.Layout.TabPageContainerView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;
        protected override bool HomeAsUp => true;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }
    }
}
