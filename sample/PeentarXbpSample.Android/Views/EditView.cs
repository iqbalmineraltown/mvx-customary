﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(SubMenuContainerViewModel)
        , Resource.Id.MainPageContainer)]
    [Register(nameof(EditView))]
    public class EditView : BaseMvxFragment<EditViewModel>
    {
        protected override int LayoutId => Resource.Layout.EditView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;

        protected override View ProcessView(View baseView)
        {
            HasOptionsMenu = true;
            return baseView;
        }
    }
}
