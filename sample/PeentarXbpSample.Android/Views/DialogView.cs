﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;

namespace PeentarXbpSample.Android.Views
{
    [MvxDialogFragmentPresentation]
    [Register(nameof(DialogView))]
    public class DialogView : BaseMvxDialog<ViewModels.DialogViewModel>
    {
        protected override int LayoutId => Resource.Layout.DialogView;

        protected override LayoutParamEnum DialogHeight => LayoutParamEnum.MatchParent;

        public DialogView() : base()
        {
        }

        public DialogView(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }
    }
}
