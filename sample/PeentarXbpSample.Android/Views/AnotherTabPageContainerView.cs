﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    /// <summary>
    /// For tab layout with fragment parent
    /// </summary>
    [MvxFragmentPresentation(typeof(SubMenuContainerViewModel), Resource.Id.MainPageContainer)]
    [Register(nameof(AnotherTabPageContainerView))]
    public class AnotherTabPageContainerView : BaseMvxFragment<AnotherTabPageContainerViewModel>
    {
        protected override int LayoutId => Resource.Layout.TabPageContainerView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;
        protected override bool HomeAsUp => true;

        protected override View ProcessView(View baseView)
        {
            var viewPager = baseView.FindViewById<ViewPager>(Resource.Id.viewpager);
            var tabs = baseView.FindViewById<TabLayout>(Resource.Id.tabs);
            if (viewPager != null)
            {
                var fragments = new List<MvxViewPagerFragmentInfo>();
                var count = 0;
                foreach (var s in ViewModel.Links)
                {
                    fragments.Add(new MvxViewPagerFragmentInfo($"Tab {++count}", typeof(AnotherTabPageView), typeof(AnotherTabPageViewModel), new { param = s }));
                }

                viewPager.Adapter = new MvxCachingFragmentStatePagerAdapter(Context, ChildFragmentManager, fragments);
                tabs.SetupWithViewPager(viewPager);
            }

            return baseView;
        }
    }
}
