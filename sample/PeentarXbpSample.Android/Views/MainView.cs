﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [Activity]
    public class MainView : BaseMvxActivity<MainViewModel>
    {
        protected override int LayoutId => Resource.Layout.MainView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;
        protected override string ActivityTitle => "Homepage";
        protected override bool HomeAsUp => false;

        public override void OnBackPressed()
        {
            var fragmentCount = SupportFragmentManager.BackStackEntryCount;
            if (fragmentCount == 0)
            {
                ViewModel.ExitCommand.Execute();
            }
            else
            {
                base.OnBackPressed();
            }
        }
    }
}
