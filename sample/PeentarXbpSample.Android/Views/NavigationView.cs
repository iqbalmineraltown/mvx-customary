﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;
using static Android.Support.Design.Widget.NavigationView;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(NavigationRootViewModel), Resource.Id.NavigationFrame)]
    [Register(nameof(NavigationView))]
    public class NavigationView : BaseMvxFragment<NavigationViewModel>, IOnNavigationItemSelectedListener
    {
        protected override int LayoutId => Resource.Layout.NavigationView;

        private global::Android.Support.Design.Widget.NavigationView _navigationView;
        private IMenuItem _previousMenuItem;

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            menuItem.SetCheckable(true);
            menuItem.SetChecked(true);
            if (menuItem != _previousMenuItem)
                _previousMenuItem?.SetChecked(false);
            _previousMenuItem = menuItem;
            Navigate(menuItem);
            return true;
        }

        private async Task Navigate(IMenuItem menuItem)
        {
            await Task.Delay(150);

            if (Activity is INavigationDrawerActivity activity)
            {
                activity.CloseDrawer();
            }
            switch (menuItem.ItemId)
            {
                case Resource.Id.first_content:
                    ViewModel.ShowFirstContentCommand.Execute();
                    break;

                case Resource.Id.second_content:
                    ViewModel.ShowSecondContentCommand.Execute();
                    break;

                case Resource.Id.third_content:
                    ViewModel.ShowThirdContentCommand.Execute();
                    break;

                default:
                    break;
            }
        }

        protected override View ProcessView(View baseView)
        {
            _navigationView = baseView.FindViewById<global::Android.Support.Design.Widget.NavigationView>(Resource.Id.navigationView);
            _navigationView.SetNavigationItemSelectedListener(this);
            return baseView;
        }

        public override void OnDestroy()
        {
            _navigationView.SetNavigationItemSelectedListener(null);
            _navigationView.Dispose();
            _navigationView = null;

            base.OnDestroy();
        }
    }
}
