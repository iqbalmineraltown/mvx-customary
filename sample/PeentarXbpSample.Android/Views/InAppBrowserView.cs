﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Attributes;
using MvvmCross.ViewModels;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(SubMenuContainerViewModel)
        , Resource.Id.MainPageContainer
        , true)]
    public class InAppBrowserView : BaseMvxFragment<InAppBrowserViewModel>, IMvxOverridePresentationAttribute
    {
        protected override int LayoutId => Resource.Layout.InAppBrowserView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;
        protected override string FragmentTitle => "In App Browser";

        public MvxBasePresentationAttribute PresentationAttribute(MvxViewModelRequest request)
        {
            var instanceRequest = request as MvxViewModelInstanceRequest;
            if (instanceRequest == null)
            {
                return null;
            }
            var viewmodel = instanceRequest.ViewModelInstance as InAppBrowserViewModel;
            return new MvxFragmentPresentationAttribute()
            {
                ActivityHostViewModelType = typeof(SubMenuContainerViewModel),
                FragmentContentId = Resource.Id.MainPageContainer,
                AddToBackStack = viewmodel.ShouldAddToBackstack,
            };
        }

        protected override View ProcessView(View baseView)
        {
            HasOptionsMenu = true;
            return baseView;
        }
    }
}
