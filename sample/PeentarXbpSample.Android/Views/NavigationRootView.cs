﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [Activity]
    public class NavigationRootView : BaseMvxActivity<NavigationRootViewModel>, INavigationDrawerActivity
    {
        protected override int LayoutId => Resource.Layout.NavigationRootView;

        public DrawerLayout DrawerLayout { get; set; }
        public MvxActionBarDrawerToggle Toggler { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            DrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    if (Toggler?.DrawerIndicatorEnabled ?? false)
                    {
                        DrawerLayout.OpenDrawer(GravityCompat.Start);
                    }
                    else
                    {
                        base.OnBackPressed();
                    }
                    return true;

                default:
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed()
        {
            if (DrawerLayout != null && DrawerLayout.IsDrawerOpen(GravityCompat.Start))
            {
                DrawerLayout.CloseDrawers();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        protected override void OnDestroy()
        {
            TearDownNavigationDrawerToggler();

            base.OnDestroy();
        }

        public bool NavDrawerSetUp => Toggler != null;

        public void SetNavDrawerEnabled(bool shouldEnable)
        {
            var lockMode = shouldEnable ? DrawerLayout.LockModeUnlocked : DrawerLayout.LockModeLockedClosed;
            DrawerLayout.SetDrawerLockMode(lockMode);
            if (NavDrawerSetUp)
                Toggler.DrawerIndicatorEnabled = shouldEnable;
        }

        public void SetupNavigationDrawerToggler(global::Android.Support.V7.Widget.Toolbar toolbar, bool homeAsUp)
        {
            TearDownNavigationDrawerToggler();
            Toggler = new MvxActionBarDrawerToggle(
                this,
                DrawerLayout,
                toolbar,
                Resource.String.drawer_open,
                Resource.String.drawer_close)
            {
                DrawerIndicatorEnabled = !homeAsUp
            };
        }

        public void TearDownNavigationDrawerToggler()
        {
            Toggler?.Dispose();
            Toggler = null;
        }

        public void SyncState()
        {
            Toggler?.SyncState();
        }

        public void CloseDrawer()
        {
            DrawerLayout.CloseDrawers();
        }
    }
}
