﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(NavigationRootViewModel), Resource.Id.ContentFrame)]
    [Register(nameof(SecondContentView))]
    public class SecondContentView : BaseMvxFragment<SecondContentViewModel>, ITestBottomSheet
    {
        private ViewGroup _testBottomSheet;

        protected override int LayoutId => Resource.Layout.SecondContentView;
        protected override bool ShouldEnableNavDrawer => true;

        protected override View ProcessView(View baseView)
        {
            _testBottomSheet = baseView.FindViewById<ViewGroup>(Resource.Id.TestBottomSheet);
            BottomSheetBehavior.From(_testBottomSheet);
            return baseView;
        }

        public bool IsExpanded
        {
            get
            {
                return BottomSheetBehavior.From(_testBottomSheet).State == BottomSheetBehavior.StateExpanded;
            }
            set
            {
                var behaviour = BottomSheetBehavior.From(_testBottomSheet);
                if (value)
                {
                    behaviour.State = BottomSheetBehavior.StateExpanded;
                }
                else
                {
                    behaviour.State = BottomSheetBehavior.StateCollapsed;
                }
            }
        }
    }

    public interface ITestBottomSheet
    {
        bool IsExpanded { get; set; }
    }
}
