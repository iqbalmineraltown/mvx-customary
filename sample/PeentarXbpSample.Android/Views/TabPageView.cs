﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Attributes;
using MvvmCross.ViewModels;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxTabLayoutPresentation(ActivityHostViewModelType = typeof(TabPageContainerViewModel)
        , TabLayoutResourceId = Resource.Id.tabs
        , ViewPagerResourceId = Resource.Id.viewpager)]
    [Register(nameof(TabPageView))]
    public class TabPageView : BaseMvxFragment<TabPageViewModel>, IMvxOverridePresentationAttribute
    {
        protected override int LayoutId => Resource.Layout.TabPageView;

        public MvxBasePresentationAttribute PresentationAttribute(MvxViewModelRequest request)
        {
            var instanceRequest = request as MvxViewModelInstanceRequest;
            if (instanceRequest == null)
            {
                return null;
            }
            var viewmodel = instanceRequest.ViewModelInstance as TabPageViewModel;
            return new MvxTabLayoutPresentationAttribute()
            {
                ActivityHostViewModelType = typeof(TabPageContainerViewModel),
                TabLayoutResourceId = Resource.Id.tabs,
                ViewPagerResourceId = Resource.Id.viewpager,
                Title = viewmodel.TabTitle,
            };
        }

        protected override View ProcessView(View baseView)
        {
            return baseView;
        }
    }
}
