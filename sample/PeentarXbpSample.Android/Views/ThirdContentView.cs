﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(NavigationRootViewModel), Resource.Id.ContentFrame)]
    [Register(nameof(ThirdContentView))]
    public class ThirdContentView : BaseMvxFragment<ThirdContentViewModel>
    {
        protected override int LayoutId => Resource.Layout.ThirdContentView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;
        protected override bool ShouldSetupNavDrawerToggler => true;
        protected override bool ShouldEnableNavDrawer => true;

        protected override View ProcessView(View baseView)
        {
            HasOptionsMenu = true;
            return baseView;
        }
    }
}
