﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    [MvxFragmentPresentation(typeof(SubMenuContainerViewModel)
        , Resource.Id.MainPageContainer)]
    [Register(nameof(ServiceView))]
    public class ServiceView : BaseMvxFragment<ServiceViewModel>
    {
        protected override int LayoutId => Resource.Layout.ServiceView;
        protected override int ToolbarId => Resource.Id.generic_toolbar;

        protected override View ProcessView(View baseView)
        {
            HasOptionsMenu = true;
            return baseView;
        }

        public override void OnResume()
        {
            base.OnResume();
            ViewModel.StartLoadCommand.Execute();
        }

        public override void OnPause()
        {
            ViewModel.CancelLoadCommand.Execute();
            base.OnPause();
        }
    }
}
