﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Xbp.Android.Core;
using PeentarXbpSample.ViewModels;

namespace PeentarXbpSample.Android.Views
{
    /// <summary>
    /// For tab layout with fragment parent
    /// </summary>
    [MvxFragmentPresentation]
    [Register(nameof(AnotherTabPageView))]
    public class AnotherTabPageView : BaseMvxFragment<AnotherTabPageViewModel>
    {
        protected override int LayoutId => Resource.Layout.TabPageView;

        protected override View ProcessView(View baseView)
        {
            return baseView;
        }
    }
}
