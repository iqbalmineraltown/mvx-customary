# Boilerplate Xamarin + MVVMCross 6

Repo ini berisi //boilerplate// yang dapat digunakan sebagai basis untuk pengembangan aplikasi //mobile// dengan Xamarin dan MVVMCross 6.
Repo berisi 3 direktori, yaitu:

1. `source`, berisi kode sumber yang nanti akan digunakan sebagai pustaka untuk proyek baru
2. `sample`, berisi contoh penggunaan dan implementasi dari pustaka
3. `projectcontents`, berisi berkas-berkas yang dibutuhkan untuk memulai proyek baru

## Panduan Penggunaan

Untuk menggunakan //boilerplate// ini silakan ikuti langkah berikut:

1. Duplikasi repo ke komputer anda
1. Buat sebuah direktori baru terpisah dari direktori repo yang baru saja diduplikasi
    - Direktori baru ini akan menjadi direktori proyek baru yang akan dikembangkan
1. Salin direktori `source` ke direktori akar dari proyek yang akan digunakan
1. Jalankan skrip `build.ps1` di direktori `source`
1. Buat solusi baru berisi proyek `PCL` dan `Android` dengan lokasi direktori di langkah 2
    - Struktur dari proyek bisa dicontoh dari proyek `sample`
    - Atau ikuti panduan di berkas `guide.md`
1. Tambahkan proyek pustaka di direktori `source` ke solusi proyek
1. Tambahkan referensi proyek pustaka ke masing-masing tipe proyek (misal: tambahkan referensi proyek `PeentarXbp` ke proyek `PCL`)
1. Salin isi direktori `projectcontents` ke direktori akar proyek
1. Pindahkan isi dari direktori `androidprojectcontents` ke direktori proyek Android
1. Sesuaikan isi dari `AppConfig.json` dengan proyek anda
1. Buka berkas `App.cs` pada project `PCL`, ganti tipe extend dari `MvxApplication` menjadi `BaseMvxApplication`

``` lang=csharp
public class App : BaseMvxApplication
```

    - Hapus juga kode berikut, karena sudah diimplementasi pada `BaseMvxApplication`

``` lang=csharp
    Mvx.LazyConstructAndRegisterSingleton<IHttpClientFactory>(() => new HttpClientFactory());
    Mvx.LazyConstructAndRegisterSingleton<IUriProvider>(() => new UriProvider());
```

12. Buka berkas `Setup.cs` pada project `Android`, ganti tipe extend dari `MvxAppCompatSetup<App>` menjadi `BaseMvxAndroidSetup<App>`

``` lang=csharp
public class Setup : BaseMvxAndroidSetup<App>
```

13. Jalankan skrip `build.ps1`
14. Lakukan pengembangan